﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickRandomSound : MonoBehaviour
{
    [SerializeField] List<AudioClip> audioClipList;
    AudioSource myAudioSource;
    private void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        myAudioSource.clip = audioClipList[GetRandomSound()];
        myAudioSource.pitch = Random.Range(0.9f, 1.5f);
        myAudioSource.Play();
    }
    int GetRandomSound()
    {
        int randomPick = Random.Range(0, audioClipList.Count);
        return randomPick;
    }
}
