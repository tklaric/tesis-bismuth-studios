﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject spiderPrefab;
    [SerializeField] GameObject harpoonWarningImage;
    [SerializeField] Transform playerTransform;

    private List<GameObject> neighborgs = new List<GameObject>();
    private List<Spider> spidersAlive = new List<Spider>();

    private bool warningTextEnabled;

    private bool spidersSpawned;
    private bool chainEventCompleted;

    public List<Spider> SpidersAlive { get => spidersAlive; set => spidersAlive = value; }
    public bool ChainEventCompleted { get => chainEventCompleted; set => chainEventCompleted = value; }
    public bool SpidersSpawned { get => spidersSpawned; set => spidersSpawned = value; }

    public void Update()
    {
        if (SpidersSpawned == true)
        {
            if (spidersAlive.Count == 0)
            {
                Invoke("EnableWarningText", 5);
                chainEventCompleted = true;
                spidersSpawned = false;
            }
           
        }
        if (warningTextEnabled)
        {
            Invoke("DisableWarningText", 7);
        }
    }

    public void EnableWarningText()
    {
        harpoonWarningImage.SetActive(true);
        warningTextEnabled = true;
    }

    public void DisableWarningText()
    {
        harpoonWarningImage.SetActive(false);
    }

    public void SpawnSpiders(List<Transform> spawnTransform,int enemyAmmount,bool isMadAtPlayer)
    {
       
        if (enemyAmmount > spawnTransform.Count)
        {
            enemyAmmount = spawnTransform.Count; //no mas de 1 araña por punto de spawn xq se ven re toscas si spawnean una arriba de la otra.
        }
        for (int i = 0; i < enemyAmmount; i++)
        {
            
            if (i > spawnTransform.Count)
            {
                i = 0;
                enemyAmmount -= spawnTransform.Count;
            }
            GameObject spiderGO = Instantiate(spiderPrefab, spawnTransform[i].position, spawnTransform[i].rotation);
            Spider spiderAux = spiderGO.GetComponent<Spider>();
            SpiderControllerWithoutSensor spiderControllerRef = spiderGO.GetComponentInChildren<SpiderControllerWithoutSensor>();
            spiderControllerRef.GetPlayerTransform(playerTransform);
            spiderAux.StartMadAtPlayer = isMadAtPlayer; //seria responsabilidad de la Spider en un  (o un OnEnable si se arma un pool) decirle a su AI que siga al jugador o que se inicie normalmente.
            spiderAux.IsChainEventSpider = true;
            spiderControllerRef.PickTarget(0);
            neighborgs.Add(spiderGO);
            SpidersAlive.Add(spiderAux);
            spiderControllerRef.Neighborgs = neighborgs;
           
        }
        SpidersSpawned = true;
    }
}
