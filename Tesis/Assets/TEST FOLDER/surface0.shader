﻿Shader "Custom/surface0"
{
	Properties
	{
		_Glossiness("Smoothness", Range(0,1)) = 0.5

		_GlassColor("Glass Color", Color) = (0,0,0,0)
		_DistortionAmount("Distortion Amount", Range(0,1)) = 0.5
		_BumpMap("Distortion Map", 2D) = "bump"{}
		_BumpScale("Normal Strength", Float) = 1

		_SpeedX("Speed X", float) = 0.5
		_SpeedY("Speed Y", float) = 0.5


		_Amplitude("Amplitude", Range(0, 1)) = 0
		_Frequency("Frequency", Range(0,10)) = 0
		_Speed("Speed", Range(-5,5)) = 0
	}
	SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

		GrabPass{}

		ZWrite Off

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows addshadow
		#pragma vertex vert
		#pragma target 3.0


		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float4 screenPos;
			float3 viewDir;
		};

		sampler2D _GrabTexture;
		float4 _GrabTexture_TexelSize;
		half _Glossiness;
		fixed4 _GlassColor;
		float _DistortionAmount;
		sampler2D _BumpMap;
		float _BumpScale;
		float _SpeedX;
		float _SpeedY;

		float _Amplitude;
		float _Frequency;
		half _Speed;

		void vert(inout appdata_full data)
		{
			float4 modifiedPos = data.vertex;
			modifiedPos.y += sin(data.vertex.x * _Frequency + _Time.y * _Speed) * _Amplitude;

			//CALCULAMOS TANGENTE
			float3 posPlusTangent = data.vertex + data.tangent * 0.01;
			posPlusTangent.y += sin(posPlusTangent.x * _Frequency + _Time.y * _Speed) * _Amplitude;
			//
			//CALCULAMOS BITANGENTE
			float3 bitangent = cross(data.normal, data.tangent);
			float3 posPlusBitangent = data.vertex + bitangent * 0.01;
			posPlusBitangent.y += sin(posPlusBitangent.x * _Frequency + _Time.y * _Speed) * _Amplitude;
			//
			//LE APLICAMOS LA POSICION MODIFICADA A LOS VALORES CALCULADOS
			float3 modifiedTangent = posPlusTangent - modifiedPos;
			float3 modifiedBitangent = posPlusBitangent - modifiedPos;

			//APLICAMOS LOS VALORES A LA NORMAL
			float3 modifiedNormal = cross(modifiedTangent, modifiedBitangent);
			data.normal = normalize(modifiedNormal);
			//
			data.vertex = modifiedPos;
		}


		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			//CALCULO LAS NORMALES
			IN.uv_BumpMap += _Time.y * float2(_SpeedX, _SpeedY);
			float3 bump = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_BumpMap), _BumpScale);

			//DISTORSIONO LOS UVs
			float2 dissort = bump * pow(_DistortionAmount * 100 + 1, 2.0);
			IN.screenPos.xy += (dissort * _GrabTexture_TexelSize.xy) * IN.screenPos.z;

			//CALCULO EL COLOR FINAL
			half3 frag;
			//frag = lerp(tex2Dproj(_GrabTexture, IN.screenPos), _GlassColor, _GlassColor.a).rgb;
			//logica original que permite manejar el alpha pero no el color.
			frag = lerp(tex2Dproj(_GrabTexture, IN.screenPos), _GlassColor, ((_GlassColor.a + _GlassColor.r + _GlassColor.g + _GlassColor.b)/4)).rgb;
			//logica rancia q permite manejar color y ALGO de alfa

			//RETORNO LOS VALORES CALCULADOS
			o.Albedo = frag;
			o.Smoothness = _Glossiness;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
