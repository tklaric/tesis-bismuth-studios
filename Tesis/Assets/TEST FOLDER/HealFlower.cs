﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealFlower : MonoBehaviour
{
    private SphereCollider mySphereCollider;
    [SerializeField] float healAmmount;
    [SerializeField] KeyCode pickUpKey;
    [SerializeField] GameObject flowerText;
    [SerializeField] Player playerRef;

    private void Awake()
    {
        mySphereCollider = GetComponent<SphereCollider>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(pickUpKey))
        {
            //EFECTO DE HEAL
            playerRef.PlayerHeal(healAmmount);
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        flowerText.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        flowerText.SetActive(false);
    }
    private void OnDisable()
    {
        if (flowerText == null) return;
        flowerText.SetActive(false);
    }
}
