﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToGround : MonoBehaviour
{
    private Quaternion smoothTilt = new Quaternion();
    [SerializeField] Vector3 offset;

    private void Awake()
    {
        RaycastHit rcHit;
        Vector3 theRay = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position + new Vector3(0, 0.1f, 0), theRay, out rcHit, 50f))
        {

            float GroundDis = rcHit.distance;
            Quaternion grndTilt = Quaternion.FromToRotation(Vector3.up, rcHit.normal);
            smoothTilt = Quaternion.Slerp(smoothTilt, grndTilt, Time.deltaTime * 2.0f);

            transform.rotation = smoothTilt * transform.rotation;

            Vector3 locPos = transform.localPosition;
            locPos.y = (transform.localPosition.y - GroundDis);
            transform.localPosition = locPos + offset;
        }
        Destroy(this);
    }
    
}
