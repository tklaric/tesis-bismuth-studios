﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsOfInterest : MonoBehaviour
{
    public List<Transform> transformList;
    [SerializeField] PulleyManager pulleyManagerRef;

    private void Awake()
    {
        for (int i = 0; i < transformList.Count; i++) 
        {
            Transform temp = transformList[i];
            int randomIndex = Random.Range(i, transformList.Count);
            transformList[i] = transformList[randomIndex];
            transformList[randomIndex] = temp;
        }
    }
    private void Start()
    {
        for (int i = 0; i < pulleyManagerRef.pulleysList.Count; i++)
        {
            pulleyManagerRef.pulleysList[i].gameObject.transform.position = transformList[i].position;
            pulleyManagerRef.pulleysList[i].gameObject.transform.rotation = transformList[i].rotation;
        }
        for (int i = 0; i < transformList.Count; i++) 
        {
            Transform temp = transformList[i];
            int randomIndex = Random.Range(i, transformList.Count);
            transformList[i] = transformList[randomIndex];
            transformList[randomIndex] = temp;
        }

    }


}
