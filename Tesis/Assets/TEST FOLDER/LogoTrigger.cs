﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoTrigger : MonoBehaviour
{
    private Player player;

    private AudioSource audioSorce;
    [SerializeField] AudioManager audioManagerRef;
    [SerializeField] SprintTip sprintTipRef;
    [SerializeField] Image image;
    [SerializeField] float fadeSpeed;
    [SerializeField] float logoTime;
    private float timer;
    private CapsuleCollider myCapsuleCollider;
    Color auxColor = new Color();
    bool begin;
    bool end;

    private void Awake()
    {
        audioSorce = GetComponent<AudioSource>();
        myCapsuleCollider = GetComponent<CapsuleCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            player = other.GetComponentInChildren<Player>();
            begin = true;
            myCapsuleCollider.enabled = false;
            auxColor = image.color;
            player.playerMovement.Speed /= 3;
            player.playerMovement.SprintSpeed /= 3;
            audioSorce.Play();
            audioManagerRef.enabled = true;
            Invoke("EnableTipBox", 20);
        }
    }

    public void EnableTipBox()
    {
        player.SprintWarning.SetActive(true);
        sprintTipRef.enabled = true;
    }

    private void Update()
    {
        if(begin)
        {
            BeginLogoStuff();
        }
        else if(end)
        {
            EndLogoStuff();
        }
    }
    private void BeginLogoStuff()
    {
        //fade in
        auxColor.a += Time.deltaTime * (fadeSpeed * 2);

        //logoTime
        if(auxColor.a > 1)
        {
            auxColor.a = 1;
            timer += Time.deltaTime;
            if(timer > logoTime)
            {
                player.playerMovement.Speed *= 3;
                player.playerMovement.SprintSpeed *= 3;
                begin = false;
                //Trigger EndLogo()
                end = true;
            }
        }
        image.color = auxColor;
        
    }
    private void EndLogoStuff()
    {
        //fade out
        auxColor.a -= Time.deltaTime * (fadeSpeed);
        image.color = auxColor;
        if (image.color.a <= 0)
        {
            image.gameObject.SetActive(false);
            this.enabled = false;
        }
        
    }
    
}
