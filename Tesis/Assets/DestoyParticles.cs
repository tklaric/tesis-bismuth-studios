﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyParticles : MonoBehaviour
{
    public GameObject go;
    IEnumerator WaitForDestroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }

    private void Update()
    {
        StartCoroutine(WaitForDestroy());
    }
}
