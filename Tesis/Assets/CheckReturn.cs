﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckReturn : MonoBehaviour
{
    [SerializeField] BoxCollider collider;
    [SerializeField] Player player;

    private void OnTriggerEnter(Collider other)
    {
        if(player.hasPuzzleArtifact == true)
        {
            player.EnablePlayerEnemySpawner();
            Destroy(this);
        }
    }
}
