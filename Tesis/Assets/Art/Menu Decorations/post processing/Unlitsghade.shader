﻿Shader "Unlit/Unlitsghade"
{
	
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 1)
		_VRadius("Vignette Radius", Range(0, 1)) = 0.5
		_VSoft("Vignette Softness", Range(0, 1)) = 0.5
	}

		SubShader
		{
			Pass
			{
				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Color;
				float _VRadius;
				float _VSoft;

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};
				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};
				float4 frag(v2f_img i) : SV_Target //diferencia con "COLOR"?
				{
					fixed4 col = tex2D(_MainTex, i.uv);

				//el vignette
				float distFromCenter = distance(i.uv.xy, float2(0.5, 0.5));
				float vignette = smoothstep(_VRadius, _VRadius - _VSoft, distFromCenter);
				col = col * vignette * _Color;

				return col;
			}
			ENDCG
		}
		}
}
