﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuWindAudioController : MonoBehaviour
{
    private AudioSource audioSrc;
    [SerializeField] float speed;
    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        //triggerFadeAudio();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator FadeAudioOut()
    {

        while (audioSrc.volume <= 1)
        {
            audioSrc.volume -= speed;
            yield return new WaitForSeconds(0.1f);
        }
    }
    public void triggerFadeAudio()
    {
        StartCoroutine(FadeAudioOut());
    }
}
