﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Down_Column : MonoBehaviour
{
    [SerializeField] GameObject text;
    SphereCollider collider;
    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        collider = GetComponent<SphereCollider>();
    }

    private void OnTriggerStay(Collider other)
    {
        text.SetActive(true);
        if (Input.GetKeyDown(KeyCode.E))
        {           
            anim.SetTrigger("Down");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
    }


}
