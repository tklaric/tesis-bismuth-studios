﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulleyManager : MonoBehaviour
{
    public List<Pulleys> pulleysList;
    [SerializeField] GameObject crossbowGOReference;
    [SerializeField] GameObject warningText;

    public GameObject WarningText { get => warningText; set => warningText = value; }

    private void Update()
    {
        //for (int i = 0; i < pulleysList.Count; i++)
        //{
        //    if (pulleysList[i].HarpoonIsarmed == false)
        //    {
        //        warningText.SetActive(true);
        //    }
        //    else
        //    {
        //        warningText.SetActive(false);
        //    }
        //}
    }

    public void RequestCrossbowSpawn(Transform spawnlocation)
    {
        for (int i = 0; i < pulleysList.Count; i++)
        {
            if (!pulleysList[i].HarpoonIsarmed)
            {
                pulleysList[i].HarpoonIsarmed = true;
                crossbowGOReference.transform.position = spawnlocation.position;
                crossbowGOReference.transform.rotation = spawnlocation.rotation;
                crossbowGOReference.SetActive(true);
            }
            else
            {

                Debug.Log("Crossbow already active somewhere");
            }
        }
       
    }
    public void RequestCrossbowDespawn()
    {
        for (int i = 0; i < pulleysList.Count; i++)
        {
            if (pulleysList[i].HarpoonIsarmed)
            {
                pulleysList[i].HarpoonIsarmed = false;
                crossbowGOReference.SetActive(false);
            }
            else
            {
                Debug.Log("No active crossbow to despawn");
            }
        }
       
    }
}
