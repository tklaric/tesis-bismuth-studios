﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pulleys : MonoBehaviour
{
    [SerializeField] Transform crossbowSpawnPosition;
    [SerializeField] Transform bloodJarPosition;
    [SerializeField] Transform player;
    [SerializeField] GameObject eventCompletedEffect;
    [SerializeField] GameObject portalEffect;
    [SerializeField] GameObject fireBallEffect;
    [SerializeField] GameObject crossbowGO;
    [SerializeField] GameObject assembleHarpoonText;
    [SerializeField] GameObject useHarpoonText;
    [SerializeField] GameObject disarmHarpoonText;
    [SerializeField] GameObject PreBucket;
    [SerializeField] GameObject warningDecal;
    [SerializeField] CableProceduralSimple succCable;
    [SerializeField] CableProceduralSimple chainCable;
    [SerializeField] Scrollbar barCraft;
    [SerializeField] Scrollbar barDeCraft;
    [SerializeField] AudioSource audioSrc;
    [SerializeField] DragonSucc dragonSuccRef;
    [SerializeField] Player p;
    [SerializeField] GameObject myRenderer;
    [SerializeField] GameObject shootingZone;
    [SerializeField] BloodBucket bucket;
    [SerializeField] Animator WheelAnimator1;
    [SerializeField] Animator WheelAnimator2;
    [SerializeField] BoxCollider pulleyCollider;
    [SerializeField] Rune rune;
    [SerializeField] float maxDetachTime;
    [SerializeField] bool isLethalPulley;


    [SerializeField] List<Transform> spiderSpawnPoints = new List<Transform>();

    public CableProceduralSimple actualConnection;
    public PulleyManager pulleyManager;


    private CrossbowShoot crossbow;
    private BloodBucket bloodBucketConnected;
    private bool hasSuccConnected;
    private bool hasChainConnected;
    private bool _jarAlreadyConnect;
    private IHigherEnemy higherBeing;

    private SphereCollider sphereCollider;

    private GameObject warningDecalPrefab;
    private bool isPulleyBlocked;
    private bool isConnected;
    private bool harpoonIsarmed;

    private float craftTime;
    private float maxCraftTime = 5;
    private float detachTime;
    private float distanceFromPlayer;
    private float minDistanceFromPlayer = 2f;
    private float disarmTime;
    private float maxDisarmTime = 5;

    
    public IHigherEnemy HigherBeing { get => higherBeing; set => higherBeing = value; }
    public CrossbowShoot Crossbow { get => crossbow; set => crossbow = value; }
    public GameObject UseHarpoonText { get => useHarpoonText; set => useHarpoonText = value; }
    public bool HasChainConnected { get => hasChainConnected; set => hasChainConnected = value; }
    public bool HasSuccConnected { get => hasSuccConnected; set => hasSuccConnected = value; }
    public BloodBucket BloodBucketConnected { get => bloodBucketConnected; set => bloodBucketConnected = value; }
    public Transform BloodJarPosition { get => bloodJarPosition; }
    public GameObject PreBucket1 { get => PreBucket; set => PreBucket = value; }
    public bool JarAlreadyConnect { get => _jarAlreadyConnect; set => _jarAlreadyConnect = value; }
    public bool HarpoonIsarmed { get => harpoonIsarmed; set => harpoonIsarmed = value; }
    public SphereCollider SphereCollider { get => sphereCollider; set => sphereCollider = value; }
    public DragonSucc DragonSuccRef { get => dragonSuccRef; set => dragonSuccRef = value; }
    public List<Transform> SpiderSpawnPoints { get => spiderSpawnPoints; set => spiderSpawnPoints = value; }
    public GameObject WarningDecal { get => warningDecal; set => warningDecal = value; }
    public bool IsPulleyBlocked { get => isPulleyBlocked; set => isPulleyBlocked = value; }

    private void Start()
    {
        SphereCollider = GetComponent<SphereCollider>();
        Crossbow = crossbowGO.GetComponent<CrossbowShoot>();
        JarAlreadyConnect = false;
    }

    private void Update()
    {
        if (hasSuccConnected == true)
        {
            WheelAnimator1.SetBool("Rotate", true);
            WheelAnimator2.SetBool("Rotate", true);
        }
        if (HasChainConnected == true)
        {
            WheelAnimator1.SetBool("Rotate", true);
            WheelAnimator2.SetBool("Rotate", true);
            DetachTimer();
        }
        else
        {
            WheelAnimator1.SetBool("Rotate", false);
            WheelAnimator2.SetBool("Rotate", false);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SuccEventCompleted();
        }   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            if (HasChainConnected == false)
            {
                if (JarAlreadyConnect == false && bucket.hasAncientBlood == false)
                {
                    PreBucket1.SetActive(true);
                }

                if (harpoonIsarmed == false)
                {
                    
                     assembleHarpoonText.SetActive(true);
                  
                }
                if (harpoonIsarmed == true)
                {
                    UseHarpoonText.SetActive(true);
                }
            }


            if (HasChainConnected == true)
            {
                disarmHarpoonText.SetActive(true);
                UseHarpoonText.SetActive(false);
            }
        }
        
       
    }
    private void OnTriggerExit(Collider other)
    {
        barCraft.gameObject.SetActive(false);
        barDeCraft.gameObject.SetActive(false);
        PreBucket1.SetActive(false);
        disarmTime = 0;
        craftTime = 0;
        assembleHarpoonText.SetActive(false);
        UseHarpoonText.SetActive(false);
        disarmHarpoonText.SetActive(false);
       
    }
    
    private void OnTriggerStay(Collider other)
    {
        p = other.GetComponentInChildren<Player>();
        barCraft.gameObject.SetActive(false);
        barDeCraft.gameObject.SetActive(false);

        if (Input.GetMouseButtonDown(0) && JarAlreadyConnect == true && hasSuccConnected == false && harpoonIsarmed == false)
        {
            BloodBucketConnected.DisconnectFromPulley();
        }

        if (Input.GetKeyDown(KeyCode.E) && p)
        {
            if (audioSrc.isPlaying == false && harpoonIsarmed == false && p.IsZoom == false)
            {
                audioSrc.Play();                
            }           
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            if(audioSrc.isPlaying == true)
            {
                audioSrc.Stop();
            }
        }

        if (Input.GetKey(KeyCode.E) && !(hasChainConnected || hasSuccConnected))
        {
            if (isLethalPulley)
            {
                if (harpoonIsarmed == false && p.IsAiming == false && p.IsZoom == false)
                {
                    barCraft.gameObject.SetActive(true);
                    assembleHarpoonText.SetActive(false);
                    craftTime += Time.deltaTime;
                    barCraft.size = craftTime / maxCraftTime;
                    if (craftTime >= maxCraftTime)
                    {
                        if (HasChainConnected)
                        {
                            Detach();
                        }
                        actualConnection = chainCable;
                        actualConnection.EndPointTransform = crossbowSpawnPosition;
                        actualConnection.Line.enabled = true;
                        pulleyManager.RequestCrossbowSpawn(crossbowSpawnPosition);
                        Crossbow.PulleyReference = this;
                        UseHarpoonText.SetActive(true);
                        craftTime = 0;
                        Crossbow.IsLethal = true;
                        Crossbow.IsSyringe = false;
                        //Crossbow.Shootcollider.SetActive(true);
                    }
                }
            }
            else if (!isLethalPulley)
            {
                if (BloodBucketConnected != null)
                {
                    PreBucket1.SetActive(false);
                    if (harpoonIsarmed == false && p.IsAiming == false && p.IsZoom == false)
                    {
                        barCraft.gameObject.SetActive(true);
                        assembleHarpoonText.SetActive(false);
                        craftTime += Time.deltaTime;
                        barCraft.size = craftTime / maxCraftTime;
                        if (craftTime >= maxCraftTime)
                        {
                            if (HasChainConnected)
                            {
                                Detach();
                            }
                            actualConnection = succCable;
                            actualConnection.EndPointTransform = crossbowSpawnPosition;
                            actualConnection.Line.enabled = true;
                            pulleyManager.RequestCrossbowSpawn(crossbowSpawnPosition);
                            Crossbow.PulleyReference = this;
                            UseHarpoonText.SetActive(true);
                            craftTime = 0;
                            Crossbow.IsLethal = false;
                            Crossbow.IsSyringe = true;
                            //Crossbow.UseHarpoonText.SetActive(false);
                            //Crossbow.Shootcollider.SetActive(true);
                        }
                    }
                }
                else
                {
                    //TENES Q CONECTAR UNA JAR
                }
            }
        }
      
        //if (Input.GetKeyDown(KeyCode.Y))
        //{
        //    if (audioSrc.isPlaying == false && BloodBucketConnected != null && harpoonIsarmed == false && p.IsZoom == false)
        //    {
        //        audioSrc.Play();
        //    }
        //}
        //if (Input.GetKeyUp(KeyCode.Y))
        //{
        //    if (audioSrc.isPlaying == true)
        //    {
        //        audioSrc.Stop();
        //    }
        //}
        else if (Input.GetKey(KeyCode.X) && harpoonIsarmed == true && hasSuccConnected == false && p.IsAiming == false && p.IsZoom == false)
        {
            barDeCraft.gameObject.SetActive(true);
            disarmTime += Time.deltaTime;
            barDeCraft.size = disarmTime / maxDisarmTime;
            if (disarmTime >= maxDisarmTime)
            {
                pulleyManager.RequestCrossbowDespawn();
                HasSuccConnected = false;
                disarmHarpoonText.SetActive(false);
                actualConnection.Line.enabled = false;
                disarmTime = 0;
                
                
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (audioSrc.isPlaying == false && harpoonIsarmed == true && p.IsZoom == false && p.IsAiming == false)
            {
                audioSrc.Play();
            }
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            if (audioSrc.isPlaying == true)
            {
                audioSrc.Stop();
            }
        }
    }
    
    public void SetChainEnd(Transform chainEnd)
    {
        actualConnection.EndPointTransform = chainEnd;
        actualConnection.Initialize();
        //IsConnected = true;
        disarmHarpoonText.SetActive(true);
    }
    
    void CalculateDistance()
    {
        distanceFromPlayer = Vector3.Distance(player.transform.position, transform.position);
    }

    void DetachTimer()
    {
        actualConnection.Line.enabled = true;
        detachTime += Time.deltaTime;

        if (detachTime >= maxDetachTime)
        {
            Detach();
        }
    }
    private void Detach()
    {
        higherBeing.PulleyShotsReceived--;
        HasChainConnected = false;
        actualConnection.EndPointTransform = null;
        actualConnection.Line.enabled = false;
        detachTime = 0;

    }
    private void HarpoonIsArmed()
    {
        harpoonIsarmed = true;

        if (crossbowGO.activeSelf == true)
        {
            //texto de volve a apretar E para entrar al modo mira
        }
    }

    public void BlockPulley()
    {
        sphereCollider.enabled = false;
        pulleyCollider.enabled = false;
        isPulleyBlocked = true;
        //rune.EnableCompletedIndicator();
    }

    public void HideMesh()
    {
        myRenderer.SetActive(false);
    }
    public void ShowMesh()
    {
        myRenderer.SetActive(true);
    }

    public void SuccEventCompleted()
    {
        warningDecalPrefab = Instantiate(warningDecal, this.transform); // warning para el jugador cuando termina succ
        Invoke("InstantiatePortalEffect", 4);
        Invoke("InstantiateFireBallEffect", 5);
    }

    public void InstantiateEventFailedEffect()
    {
        GameObject effectGO = Instantiate(eventCompletedEffect, this.transform);
        effectGO.transform.position += new Vector3(0, 5, 0);
    }

    public void InstantiatePortalEffect()
    {
        GameObject portalEffectGO = Instantiate(portalEffect, this.transform); //rotartlo para que quede en vertical
        portalEffectGO.transform.position += new Vector3(0, 20, 0);
        portalEffectGO.transform.localScale += new Vector3(2, 2, 2);
    }

    public void InstantiateFireBallEffect()
    {
        GameObject fireBallGO = Instantiate(fireBallEffect, this.transform);
        Destroy(warningDecalPrefab);
        Debug.LogError("BOLA DE FUEGOOOOOOOOOOOOOO");
    }

}
