﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookCanvas : MonoBehaviour
{
    private MouseLook mouseLook;

    private void Awake()
    {
        mouseLook = GetComponent<MouseLook>();
    }

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        mouseLook.enabled = false;
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        mouseLook.enabled = true;
    }
}
