﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimMinigames : MonoBehaviour
{
    private List<float> results = new List<float>();
    [SerializeField] MouseLook mouseLookRef;
    [SerializeField] CrossbowShoot crossbowRef;
    [SerializeField] GameObject background1;
    [SerializeField] GameObject background3;
    [SerializeField] GameObject minigame3FeedbackText;
    [SerializeField] GradientTest gradientTestRefLock;
    [SerializeField] GradientTest gradientTestRef1;
    [SerializeField] GradientTest gradientTestRef2;
    [SerializeField] GradientTest gradientTestRef3;
    [SerializeField] LayerMask ignoreLayer;
    [SerializeField] float speedMultiplier;
    [SerializeField] float maxTimeForLockValue;
    private float minigameFloat1;
    private float minigameFloat2;
    private float minigameFloat3;
    private float timeForLock;
    private bool isLocked;
    private bool probabilitySuccess;
    private bool minigame1Completed;
    private bool minigame2Completed;
    private bool minigame3Completed;
    public bool allGamesCompleted;

    public bool IsLocked { get => isLocked; set => isLocked = value; }
    public bool ProbabilitySuccess { get => probabilitySuccess; set => probabilitySuccess = value; }

    private void Awake()
    {
        
    }
    private void Update()
    {
        if(!IsLocked) //estoy apuntando pero todavia no lockie
        {
            
            RaycastHit hit;
            if (Physics.Raycast(crossbowRef.ArrowLocation.position, crossbowRef.ArrowLocation.transform.forward, out hit, 1000, ~ignoreLayer))
            {
                if (hit.collider.gameObject.layer == 11)
                {
                    timeForLock += Time.deltaTime;
                    if (timeForLock >= maxTimeForLockValue)
                    {
                        IsLocked = true;
                        mouseLookRef.ObtainTarget(hit.collider.gameObject.GetComponentInParent<Dragon>().GiveTarget());
                    }
                }
                else
                {
                    timeForLock -= Time.deltaTime;

                    if (timeForLock <= 0)
                    {
                        timeForLock = 0;
                    }
                }
                
            }
            else
            {
                timeForLock -= Time.deltaTime;
                
                if (timeForLock <= 0)
                {
                    timeForLock = 0;
                }
            }
            gradientTestRefLock.FillImage(timeForLock, maxTimeForLockValue);
        }
        else
        {
            gradientTestRefLock.CompleteLock();

            
            if (!minigame1Completed)
            {
                Minigame1();
                
            }
            else
            {
                if (!minigame2Completed)
                {
                    Minigame2();
                }
                else
                {
                    if(!minigame3Completed)
                    {
                        Minigame3();
                    }
                    else
                    {
                        allGamesCompleted = true;
                        //Chekeo en la list de resultados y disparo
                    }
                }
            }
            //minigames
        }

        

    }
    private void Minigame1()
    {
        background1.SetActive(true);
        minigameFloat1 += Time.deltaTime * speedMultiplier;
        if(minigameFloat1 > 1)
        {
            minigameFloat1 = 0;
        }
        gradientTestRef1.FillImage(minigameFloat1, 1);
        if(Input.GetMouseButtonDown(0))
        {
            results.Add(minigameFloat1);
            minigame1Completed = true;
        }
    }
    private void Minigame2()
    {
        minigame2Completed = true;
        results.Add(1);
        //float minigameFloat2 = Mathf.Sin(Time.time * speedMultiplier);
        //if(minigameFloat2 > 0)
        //{
        //    minigameFloat2 -= 1;
        //}
        //else if(minigameFloat2 <= 0)
        //{
        //    minigameFloat2 += 1;
        //}
        //minigameFloat2 = Mathf.Abs(minigameFloat2);

        //gradientTestRef2.FillImage(minigameFloat2, 1);  //REVISAR COMO LLENAR LA BARRA PARA MOSTRAR Q LA IDEA ES FRENAR CERCA DEL 0
        //if (Input.GetMouseButtonDown(0))
        //{
        //    results.Add(minigameFloat2);
        //    minigame2Completed = true;
        //}
    }
    private void Minigame3()
    {
        background3.SetActive(true);

        minigame3FeedbackText.SetActive(true);

        float resistanceMultiplier = 1; //cambiar este valor si al inicio es muy sidoso
        if (minigameFloat3 < 0.05f)
        {
            minigameFloat3 = 0.05f;
        }
        resistanceMultiplier = 2 + minigameFloat3*2;

        minigameFloat3 -= (Time.deltaTime / 10) * resistanceMultiplier;


        if (Input.GetKeyUp(KeyCode.Space))
        {
            minigameFloat3 += 0.08f;
            if (minigameFloat3 > 1)
            {
                minigameFloat3 = 1;
            }

        }
        else if (Input.GetMouseButtonDown(0))
        {
            results.Add(minigameFloat3);
            minigame3Completed = true;
            AnalyzeResults();
        }
        gradientTestRef3.FillImage(minigameFloat3, 1);  //REVISAR COMO LLENAR LA BARRA PARA MOSTRAR Q LA IDEA ES FRENAR CERCA DEL 0
        
    }
    private void AnalyzeResults()
    {
        float totalResult = 0;

        for (int i = 0; i < results.Count; i++)
        {
            totalResult += results[i]; //calculamos el resultado total de los minigames
        }

        totalResult /= results.Count; //obtenemso numero del 0 al 1

        float random = Random.Range(0.33f, 1f); //cambiar si se agrega un 2do minijuego

        if (random<=totalResult)
        {

            probabilitySuccess = true;
            //fracasar. eventualmente dragon tiene 2 seeks uno arriba y el otro abajo y le va a apuntar a eso
        }
        else
        {
            
            probabilitySuccess = false; //succes el tiro va hacia el seek del dragon
        }
        allGamesCompleted = true;
    }

   


    private void OnDisable()
    {
        minigame3FeedbackText.SetActive(false);
        background1.SetActive(false);
        background3.SetActive(false);
        IsLocked = false;
        timeForLock = 0;
        mouseLookRef.ForgetTarget();
        gradientTestRefLock.FillImage(0, maxTimeForLockValue);
        gradientTestRef1.FillImage(0, maxTimeForLockValue);
        //gradientTestRef2.FillImage(0, maxTimeForLockValue);
        gradientTestRef3.FillImage(0, maxTimeForLockValue);
        gradientTestRefLock.CompleteLock();
        minigame1Completed = false;
        minigame2Completed = false;
        minigame3Completed = false;
        minigameFloat1 = 0;
        minigameFloat2 = 0;
        minigameFloat3 = 0;
        results = new List<float>();
        //deactivate UI
        allGamesCompleted = false;
    }
    
}
