﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysPointsOfInterest : MonoBehaviour
{
    [SerializeField] List<Transform> transformList = new List<Transform>();
    [SerializeField] List<GameObject> enemiesPrefabs = new List<GameObject>();

    [SerializeField] int blueManticoraAmmount;
    [SerializeField] int greenManticoraAmmount;
    [SerializeField] int redManticoraAmmount;
    [SerializeField] int yellowManticoraAmmount;

    [SerializeField] int spiderAmmount;

    private void Awake()
    {
        for (int i = 0; i < transformList.Count; i++)
        {
            Transform temp = transformList[i];
            int randomIndex = Random.Range(i, transformList.Count);
            transformList[i] = transformList[randomIndex];
            transformList[randomIndex] = temp;
        }
    }

    private void Start()
    {
        for (int i = 0; i < blueManticoraAmmount; i++)
        {
            GameObject manticoraGO = Instantiate(enemiesPrefabs[0]);
            manticoraGO.transform.position = transformList[i].transform.position; //ENCONTRAR LA MANERA DE QUE QUEDE LA COLA AFUERA NOMAS
            manticoraGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < greenManticoraAmmount; i++)
        {
            GameObject greenManticoraGO = Instantiate(enemiesPrefabs[1]);
            greenManticoraGO.transform.position = transformList[i].transform.position; //ENCONTRAR LA MANERA DE QUE QUEDE LA COLA AFUERA NOMAS
            greenManticoraGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < redManticoraAmmount; i++)
        {
            GameObject redManticoraGO = Instantiate(enemiesPrefabs[2]);
            redManticoraGO.transform.position = transformList[i].transform.position; //ENCONTRAR LA MANERA DE QUE QUEDE LA COLA AFUERA NOMAS
            redManticoraGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < yellowManticoraAmmount; i++)
        {
            GameObject yellowManticoraGO = Instantiate(enemiesPrefabs[3]);
            yellowManticoraGO.transform.position = transformList[i].transform.position; //ENCONTRAR LA MANERA DE QUE QUEDE LA COLA AFUERA NOMAS
            yellowManticoraGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < spiderAmmount; i++)
        {
            GameObject spiderGO = Instantiate(enemiesPrefabs[1]);
            spiderGO.transform.position = transformList[i].transform.position;
            spiderGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }
    }

}
