﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPagesSpawnPoints : MonoBehaviour
{

    [SerializeField] List<Transform> bookPagesSpawnPoints = new List<Transform>();
    [SerializeField] List<BookPagePickUp> bookPages = new List<BookPagePickUp>();


    private void Awake()
    {
        for (int i = 0; i < bookPagesSpawnPoints.Count; i++)
        {
            Transform temp = bookPagesSpawnPoints[i];
            int randomIndex = Random.Range(i, bookPagesSpawnPoints.Count);
            bookPagesSpawnPoints[i] = bookPagesSpawnPoints[randomIndex];
            bookPagesSpawnPoints[randomIndex] = temp;
        }
    }

    private void Start()
    {
        for (int i = 0; i < bookPages.Count; i++)
        {
            bookPages[i].gameObject.transform.position = bookPagesSpawnPoints[i].transform.position;
        }
    }
}
