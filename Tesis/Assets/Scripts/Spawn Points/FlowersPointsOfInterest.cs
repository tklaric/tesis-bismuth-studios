﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowersPointsOfInterest : MonoBehaviour
{
    [SerializeField] List<Transform> transformList = new List<Transform>();
    [SerializeField] List<GameObject> flowersPrefabs = new List<GameObject>();

    [SerializeField] int resistanceFlowerAmmount;
    [SerializeField] int pulleyResistanceFlowerAmmount;
    [SerializeField] int shotgunFlowerAmmount;
    [SerializeField] int succSpeedFlowerAmmount;


    private void Awake()
    {
        for (int i = 0; i < transformList.Count; i++)
        {    
            Transform temp = transformList[i];
            int randomIndex = Random.Range(i, transformList.Count);
            transformList[i] = transformList[randomIndex];
            transformList[randomIndex] = temp;
        }
    }

    private void Start()
    {
        for (int i = 0; i < pulleyResistanceFlowerAmmount; i++)
        {
            GameObject pulleyResistanceFlowerGO = Instantiate(flowersPrefabs[0]);
            pulleyResistanceFlowerGO.transform.position = transformList[i].transform.position;
            //pulleyResistanceFlowerGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < resistanceFlowerAmmount; i++)
        {
            GameObject resistanceFlowerGO = Instantiate(flowersPrefabs[1]);
            resistanceFlowerGO.transform.position = transformList[i].transform.position;
            //resistanceFlowerGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        } 

        for (int i = 0; i < shotgunFlowerAmmount; i++)
        {
            GameObject shotgunFlowerGO = Instantiate(flowersPrefabs[2]);
            shotgunFlowerGO.transform.position = transformList[i].transform.position;
            //shotgunFlowerGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

        for (int i = 0; i < succSpeedFlowerAmmount; i++)
        {
            GameObject succSpeedFlowerGO = Instantiate(flowersPrefabs[3]);
            succSpeedFlowerGO.transform.position = transformList[i].transform.position;
            //succSpeedFlowerGO.transform.rotation = transformList[i].transform.rotation;
            transformList.RemoveAt(i);
        }

    }

}
