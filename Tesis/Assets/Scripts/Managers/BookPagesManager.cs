﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPagesManager : MonoBehaviour
{

    [SerializeField] Book bookRef;
    [SerializeField] List<Sprite> narrativePages = new List<Sprite>();

    private int counter = 10;
    private int counterAux = 0;

    public void PickUpPages()
    {
      

        bookRef.bookPages[counter] = narrativePages[counterAux];
        bookRef.spritesB[counter] = narrativePages[counterAux];
        counter++;
        counterAux++;

    }

}
