﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    [SerializeField] Player playerTest;
    [SerializeField] PlayerMovement player;
    [SerializeField] DragonSucc dragonSuccRef;
    [SerializeField] BloodBucket bloodBucketRef;
    [SerializeField] Dragon drake;

    private void Awake()
    {
        //LoadData();
    }

    public void LoadData()
    {
        SaveData.current = (SaveData)SerializationManager.Load(Application.persistentDataPath + "/saves/SaveData.save");
        dragonSuccRef.SuccsCompleted = SaveData.current._succsCompleted;
        bloodBucketRef.actualBloodLiters = SaveData.current._actualBloodLitters;
        bloodBucketRef.BloodFill.transform.localScale = SaveData.current._bloodSize;
        player.transform.position = SaveData.current._playerPosition;
        playerTest.hasPuzzleArtifact = SaveData.current._hasPuzzleArtifact;
        dragonSuccRef.HigherBloodBar.size = SaveData.current._higherBloodBar;
        drake.DragonDied = SaveData.current._dragonDied;
        drake.transform.position = SaveData.current.dragonPosition;

        //TIENE QUE HABER NA MANERA MAS OPTIMA PARA NO ANDAR IGUALANDO TODO
    }
}
