﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    [SerializeField] Animator fadeAnim;

    public Animator FadeAnim { get => fadeAnim; set => fadeAnim = value; }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    private void Awake()
    {
        fadeAnim.SetTrigger("FadeIn");
        //Cursor.visible = false;
        //PlayerPrefs.DeleteAll();
    }

    public void Quit()
    {
        Application.Quit();
        
    }

    public void FadeOut()
    {
        fadeAnim.SetTrigger("FadeOut");
        Invoke("ChangeToLevel", 1.5f);
    }

   private void ChangeToLevel()
    {
        LoadScene("SampleScene");
    }

    public void UnlockedCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
