﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    [SerializeField] AudioSource audioSrc;
    [SerializeField] AudioMixer backgroundMusicMixer;
    [SerializeField] DragonSucc dragonSuccRef;
    [SerializeField] List<AudioClip> backgroundMusic = new List<AudioClip>();
    [SerializeField] AudioClip artifactObtainedMusic;
    [SerializeField] AudioClip dragonSlaughteredMusic;
    [SerializeField] AudioClip combatMusic;
    [SerializeField] Artifact artifactRef;
    [SerializeField] Player playerRef;
    [SerializeField] Dragon dragonRef;
    [SerializeField] EnemySpawner enemySpawnerRef;

    private float timer;
    private float masterVolume = -10;
    private float minAudioMixerVolume = -30;
    private float maxAudioMixerVolume = -10;

    private bool chainEventCompleted;
    private bool succEventCompleted;
    private bool clipHasChanged;
    private bool decreasing = true;

    void Update()
    {
       
        StartAmbienceMusic();
        StartArtifactMusic();
        StartSlaughterMusic();
        StartCombatMusic();
    }

    void StartAmbienceMusic()
    {
        if (!audioSrc.isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= 120)
            {
                int random = Random.Range(0, backgroundMusic.Count);
                audioSrc.clip = backgroundMusic[random];
                audioSrc.Play();

                timer = 0;
            }
        }
        
    }

    void StartArtifactMusic()
    {
        if (artifactRef.AltarHasRised)
        {
            if (audioSrc.isPlaying && audioSrc.clip != artifactObtainedMusic && clipHasChanged == false && decreasing == true) 
            {
                masterVolume -= 5 * Time.deltaTime;
                if (masterVolume <= minAudioMixerVolume) 
                {
                    masterVolume = minAudioMixerVolume;
                    audioSrc.clip = artifactObtainedMusic;
                    audioSrc.Play();
                    clipHasChanged = true;
                    decreasing = false;
                }
                
                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);      

            }
            else
            {
                if (!audioSrc.isPlaying)
                {
                    audioSrc.clip = artifactObtainedMusic;
                    audioSrc.Play();
                }
                
            }
            if (decreasing == false)
            {
                masterVolume += 5 * Time.deltaTime;
                
                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

                if (masterVolume >= maxAudioMixerVolume)
                {
                    masterVolume = maxAudioMixerVolume;
                   
                }
            }         
            
        }
    }

    void StartSlaughterMusic()
    {
        if (playerRef.DragonSlaughtered)
        {
            if (audioSrc.isPlaying && audioSrc.clip != dragonSlaughteredMusic && clipHasChanged == false && decreasing == true)
            {
                masterVolume -= 5 * Time.deltaTime;
                if (masterVolume <= minAudioMixerVolume)
                {
                    masterVolume = minAudioMixerVolume;
                    audioSrc.clip = dragonSlaughteredMusic;
                    audioSrc.Play();
                    clipHasChanged = true;
                    decreasing = false;
                }

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

            }
            else
            {
                if (!audioSrc.isPlaying)
                {
                    audioSrc.clip = dragonSlaughteredMusic;
                    audioSrc.Play();
                }

            }
            if (decreasing == false)
            {
                masterVolume += 5 * Time.deltaTime;

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

                if (masterVolume >= maxAudioMixerVolume)
                {
                    masterVolume = maxAudioMixerVolume;

                }
            }

        }
    }

    void StartCombatMusic()
    {
        if (dragonSuccRef.enabled == true || enemySpawnerRef.SpidersSpawned == true)
        {
            if (audioSrc.isPlaying && audioSrc.clip != combatMusic && clipHasChanged == false && decreasing == true)
            {
                masterVolume -= 5 * Time.deltaTime;
                if (masterVolume <= minAudioMixerVolume)
                {
                    masterVolume = minAudioMixerVolume;
                    audioSrc.clip = combatMusic;
                    audioSrc.Play();
                    clipHasChanged = true;
                    decreasing = false;
                }

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

            }
            else
            {
                if (!audioSrc.isPlaying)
                {
                    audioSrc.clip = combatMusic;
                    audioSrc.Play();
                }

            }
            if (decreasing == false)
            {
                masterVolume += 5 * Time.deltaTime;

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

                if (masterVolume >= maxAudioMixerVolume)
                {
                    masterVolume = maxAudioMixerVolume;
                    decreasing = true;
                }
            }
            
        }

        ChainEventCompleted();
        SuccEVentCompleted();
     
        
    }

    public void SuccEVentCompleted()
    {
        if (dragonSuccRef.BloodBucketReference.hasAncientBlood == true)
        {
            if (succEventCompleted == false)
            {
                masterVolume -= 5 * Time.deltaTime;

                if (masterVolume <= minAudioMixerVolume)
                {
                    masterVolume = minAudioMixerVolume;
                    audioSrc.Stop();
                    decreasing = false;
                    succEventCompleted = true;
                }
            }

            backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

            if (decreasing == false)
            {
                masterVolume += 5 * Time.deltaTime;

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

                if (masterVolume >= maxAudioMixerVolume)
                {
                    masterVolume = maxAudioMixerVolume;
                    succEventCompleted = false;
                }
            }
        }
    } 

    public void ChainEventCompleted()
    {
        if (enemySpawnerRef.ChainEventCompleted == true)
        {
            if (chainEventCompleted == false)
            {
                masterVolume -= 5 * Time.deltaTime;

                if (masterVolume <= minAudioMixerVolume)
                {
                    masterVolume = minAudioMixerVolume;
                    audioSrc.Stop();
                    decreasing = false;
                    chainEventCompleted = true;
                }
            }

            backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

            if (decreasing == false)
            {
                masterVolume += 5 * Time.deltaTime;

                backgroundMusicMixer.SetFloat("BackgroundMusicVolume", masterVolume);

                if (masterVolume >= maxAudioMixerVolume)
                {
                    masterVolume = maxAudioMixerVolume;
                    enemySpawnerRef.ChainEventCompleted = false;
                    chainEventCompleted = false;
                }
            }
        }
    }

}
