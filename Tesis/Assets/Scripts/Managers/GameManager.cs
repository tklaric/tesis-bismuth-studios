﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject menu;
    [SerializeField] Player player;
    [SerializeField] LevelManager levelManager;
    [SerializeField] DragonSucc dragonSuccRef;
    [SerializeField] Dragon dragon;
    [SerializeField] SpiderSpawner spiderSpawner;
    [SerializeField] GameObject buggyLightBeam;
    [SerializeField] Transform buggyTransform;
    [SerializeField] AudioSource audioSrc;
    [SerializeField] AudioClip deathSound;
    [SerializeField] MouseLook mouseLookRef;

    private Pulleys pulley;

    private bool IsOnPause;
    private float spawnTimer;
    private float distanceFromBuggy;


    public Player Player { get => player; set => player = value; }
    public Pulleys Pulley { get => pulley; set => pulley = value; }

    private void Awake()
    {
        //SaveData.current = (SaveData)SerializationManager.Load(Application.persistentDataPath + "/saves/PlayerSave.save");
        //playermov.transform.position = SaveData.current.playerPosition;
        //DontDestroyOnLoad(this);
    }

    void Start()
    {
        IsOnPause = false;
        dragonSuccRef.spiderSpawnerRef = spiderSpawner;
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    SaveData.current.playerPosition = playermov.transform.position;
        //    SerializationManager.Save("PlayerSave", SaveData.current);
        //}
         
       
          
      
        CalculateDistance();

        CheckPlayerStats();

        if (dragonSuccRef.enabled == true)
        {
            spawnTimer += Time.deltaTime;
        }


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsOnPause)
            {
                ContinueGameButton();
                
            }
            else
            {
                menu.SetActive(true);
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                IsOnPause = true;
                mouseLookRef.pauseModeSensitivity();
            }
        }

        if (spawnTimer >= 5f)
        {
            SpawnTrigger();
            spawnTimer = 0;
        }

    }

    public void SpawnTrigger()
    {
        if (Pulley == null) return;
        spiderSpawner.SpawnPoints = Pulley.SpiderSpawnPoints;
        spiderSpawner.SpawnGroup(3);

    }

    public void ContinueGameButton()
    {
        Time.timeScale = 1;
        menu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        IsOnPause = false;
        mouseLookRef.resumeSensitivity();
    }

    public void ExitGameButton()
    {
        Time.timeScale = 1;
    }

    private void CheckPlayerStats()
    {
        if (Player.life <= 0)
        {
            levelManager.UnlockedCursor();
            levelManager.LoadScene("Death");
            audioSrc.clip = deathSound;
            audioSrc.Play();
        }

        if (player.DragonSlaughtered)
        {
            buggyLightBeam.SetActive(true);
        }
    }


    private void CalculateDistance()
    {
        distanceFromBuggy = Vector3.Distance(player.transform.position, buggyTransform.position);

        if (distanceFromBuggy <= 50) 
        {
            buggyLightBeam.SetActive(false);
        }
    }

}
