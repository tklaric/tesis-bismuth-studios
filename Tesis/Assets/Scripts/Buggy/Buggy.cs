﻿using UnityEngine;

public class Buggy : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] GameObject text;
    [SerializeField] LevelManager _levelmanager;
    [SerializeField] AudioClip buggyStartUp;
    [SerializeField] string sceneName;

    private AudioSource audioSource;

    float distanceFromPlayer;
    float minDistanceFromPlayer = 3f;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        CalculateDistance();
        CheckDIstance();
    }

    void CheckDIstance()
    {
        if (distanceFromPlayer <= minDistanceFromPlayer && (player.hasPuzzleArtifact == true || player.DragonSlaughtered == true))
        {
            text.SetActive(true);

            if (Input.GetKey(KeyCode.E))
            {
                audioSource.Play();

                Destroy(text.gameObject);

                _levelmanager.FadeAnim.SetTrigger("FadeOut");

                if (player.DragonSlaughtered == true)
                {
                    Invoke("ChangeToBadEnding", 1.5f);
                }

                if (player.hasPuzzleArtifact == true)
                {
                    Invoke("ChangeToGoodEnding", 1.5f);
                }
              
               
                
            }
        }
        else text.SetActive(false);
    }

    private void ChangeToBadEnding()
    {
        _levelmanager.LoadScene("Bad Ending");
        _levelmanager.UnlockedCursor();
    }

    private void ChangeToGoodEnding()
    {
        _levelmanager.LoadScene("Good Ending");
        _levelmanager.UnlockedCursor();
    }

    void CalculateDistance()
    {
        distanceFromPlayer = Vector3.Distance(player.transform.position, transform.position);
    }
}
