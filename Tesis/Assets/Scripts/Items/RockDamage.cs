﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockDamage : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponentInChildren<Player>();
        if (p != null)
        {
            p.PlayerTakeDamage(50);
            Debug.Log("DANIADOO");
        }
    }
}