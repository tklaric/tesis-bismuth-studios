﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeFlower : MonoBehaviour
{

    [SerializeField] float damageAmmount;
    [SerializeField] KeyCode pickUpKey;
    [SerializeField] GameObject fakeFlowerText;

    private Player playerRef;
    private SphereCollider mySphereCollider;
    private bool hasBeenConsumed;

    public bool HasBeenConsumed { get => hasBeenConsumed; set => hasBeenConsumed = value; }

    private void Awake()
    {
        mySphereCollider = GetComponent<SphereCollider>();
    }

    private void OnTriggerStay(Collider other)
    {
        playerRef = other.GetComponentInChildren<Player>();

        if (Input.GetKeyDown(pickUpKey) && HasBeenConsumed == false) 
        {
            playerRef.PlayerTakeDamage(damageAmmount);
            HasBeenConsumed = true;
            this.enabled = false;
            mySphereCollider.enabled = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        playerRef = other.GetComponentInChildren<Player>();
        playerRef.FlowerText.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
       // playerRef = other.GetComponentInChildren<Player>();
        playerRef.FlowerText.SetActive(false);
    }
    private void OnDisable()
    {
        if (playerRef == null) return;
        playerRef.FlowerText.SetActive(false);
    }
}
