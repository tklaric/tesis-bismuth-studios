﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockMovement : MonoBehaviour
{

    private Vector3 portalPosition;
    private float speed = 10;

    public Vector3 PortalPosition { get => portalPosition; set => portalPosition = value; }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, portalPosition,speed*Time.deltaTime);
        if (Vector3.Distance(transform.position, portalPosition) <= 1)
        {
            Destroy(this.gameObject);
        } 
    }



}
