﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPagePickUp : MonoBehaviour
{

    [SerializeField] AudioSource audioSrc;
    [SerializeField] GameObject interactText;


    [SerializeField] BookPagesManager BookPagesManagerRef;

    private bool pickedUpPage;

    private void OnTriggerStay(Collider other)
    {
        Player player = other.GetComponentInChildren<Player>();

        if (pickedUpPage == false)
        {
            interactText.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            audioSrc.Play();

            BookPagesManagerRef.PickUpPages();

            interactText.SetActive(false);
            pickedUpPage = true;
            Destroy(this.gameObject,1);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        interactText.SetActive(false);
    }

   


}
