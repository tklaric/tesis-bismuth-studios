﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtleArmMovement : MonoBehaviour
{
    [SerializeField] float minMax =1;
    private Vector3 movementVector;
    [SerializeField] PlayerMovement playerMovementRef;
    private float playerVelocityFloat;
    
    // Update is called once per frame
    void Update()
    {
        playerVelocityFloat = (Mathf.Clamp((Mathf.Abs(playerMovementRef.move.x) + Mathf.Abs(playerMovementRef.move.z)),-999,1) / 2)/10;
        transform.localPosition = new Vector3(playerVelocityFloat*minMax, -playerVelocityFloat*minMax, 0f);
    }
}
