﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Player : MonoBehaviour
{
    [SerializeField] MouseLook cameraLook;
    [SerializeField] CanvasDamage bloodCanvas;
    [SerializeField] Camera camera;
    [SerializeField] Transform player;
    [SerializeField] Transform dragonTransform;
    [SerializeField] GameObject slaughterText;
    [SerializeField] GameObject flowerText;
    [SerializeField] TextMeshProUGUI buffTimerText;
    [SerializeField] AudioSource audioSrc;
    [SerializeField] AudioSource heartbeatSrc;
    [SerializeField] AudioClip slaughterSound;
    [SerializeField] AudioClip heartbeatSound;
    [SerializeField] AudioClip recoveringSound;
    [SerializeField] Scrollbar slaughterBar;
    [SerializeField] CrossbowShoot crossBow;
    [SerializeField] float maxStamina;
    [SerializeField] float maxCooldownToRegenerate;
    [SerializeField] float mouseSensitivity;
    [SerializeField] float speed;
    [SerializeField] float sprintSpeed;
    [SerializeField] GameObject sprintWarning;
    [SerializeField] GameObject playerSpiderSpawnerRef;
    Color sprintColor;

    [SerializeField] List<AudioClip> damagedSounds = new List<AudioClip>();

    public PlayerMovement playerMovement;
    public ToolWheelInput inputWheelReference;

    Rigidbody rb;

    int attachedCounter;

    int resistanceFlower;
    int pulleyResistanceFlower;
    int shotgunFlower;
    int succSpeedFlower;


    float distanceFromDragon;
    float minDistanceFromDragon = 3f;
    float equipedSpeed = 3f;
    float yRotSpeed = 20f;
    float damage;
    float criticalLife = 40;
    float shotPower = 1000f; 
    float rotationCounter;  
    float slaughterTime;
    float maxSlaughterTime = 5;
    float maxRegenLife = 81;
    float currentStamina;
    float cooldownToRegen = 0;
    float initialFOV;
  

    public float life = 100f;
    public bool hasPuzzleArtifact;

    bool dragonSlaughtered;
    bool isSprinting;
    bool[] carUpgrades;
    bool isZoom = false;
    bool isAiming = false;
    bool hasResistanceBuffed;

    public bool IsAiming { get => isAiming; set => isAiming = value; }
    public float Speed { get => speed; set => speed = value; }
    public bool[] CarUpgrades { get => carUpgrades; set => carUpgrades = value; }
    public bool IsSprinting { get => isSprinting; set => isSprinting = value; }
    public float Life { get => life; set => life = value; }
    public float CurrentStamina { get => currentStamina; set => currentStamina = value; }
    public bool IsZoom { get => isZoom; set => isZoom = value; }
    public float CooldownToRegen { get => cooldownToRegen; set => cooldownToRegen = value; }
    public float MaxCooldownToRegenerate { get => maxCooldownToRegenerate; set => maxCooldownToRegenerate = value; }
    public bool DragonSlaughtered { get => dragonSlaughtered; set => dragonSlaughtered = value; }
    public bool HasResistanceBuffed { get => hasResistanceBuffed; set => hasResistanceBuffed = value; }
    public int ResistanceFlower { get => resistanceFlower; set => resistanceFlower = value; }
    public int PulleyResistanceFlower { get => pulleyResistanceFlower; set => pulleyResistanceFlower = value; }
    public int ShotgunFlower { get => shotgunFlower; set => shotgunFlower = value; }
    public int SuccSpeedFlower { get => succSpeedFlower; set => succSpeedFlower = value; }
    public GameObject FlowerText { get => flowerText; set => flowerText = value; }
    public TextMeshProUGUI BuffTimerText { get => buffTimerText; set => buffTimerText = value; }
    public AudioSource HeartbeatSrc { get => heartbeatSrc; set => heartbeatSrc = value; }
    public GameObject SprintWarning { get => sprintWarning; set => sprintWarning = value; }

    private void Awake()
    {
        playerMovement = GetComponentInParent<PlayerMovement>();
      
    }

    private void Start()
    {
        initialFOV = Camera.main.fieldOfView;
        inputWheelReference = GetComponent<ToolWheelInput>();
        rb = GetComponent<Rigidbody>();
        camera = Camera.main;
        player = GetComponent<Transform>();
        //hasPuzzleArtifact = false;
        IsSprinting = false;
        CurrentStamina = maxStamina;
        cooldownToRegen = maxCooldownToRegenerate;
    }

    void Update()
    {
      
        RegenerateLife();
        CalculateDistance();
        SlaughterDragon();
       
    }

    public void PlayerTakeDamage(float damage)
    {
        int random = Random.Range(0, damagedSounds.Count);

        if(hasResistanceBuffed)
        {
            bloodCanvas.Bleeding();
            life -= damage / 2;

            //if (!audioSrc.isPlaying) //CONSULTAR CON NICO COMO EVITAR QUE LOS SONIDOS SE PISEN 
            //{
                audioSrc.clip = damagedSounds[random];
                audioSrc.Play();
            //}
        }
        else
        {
            bloodCanvas.Bleeding();
            life -= damage;
            
            //if (!audioSrc.isPlaying)
            //{
                audioSrc.clip = damagedSounds[random];
                audioSrc.Play();
            //}
        }
        

        if (life<=criticalLife)
        {
            HeartbeatSrc.clip = heartbeatSound;
            HeartbeatSrc.Play();
        }
    }

    public void PlayerHeal(float healAmmount)
    {
        life += healAmmount;
    }
       
    public void NoSprinting()
    {
        IsSprinting = false;
        //sprintColor.a -= Time.deltaTime / 1.5f;
        //if(sprintColor.a <= 0)
        //{
        //    sprintColor.a = 0;
        //}
        //sprintImage.color = sprintColor;

        if (currentStamina < maxStamina)
        {
            RegenerateStamina();    
        }
        if(currentStamina >= maxStamina)
        {
            cooldownToRegen = 0;
        }
        if(!IsAiming)
        {
            Camera.main.fieldOfView -= Time.deltaTime * 15;
            if (Camera.main.fieldOfView < initialFOV)
            {
                Camera.main.fieldOfView = initialFOV;
            }
        }
    }

    public void Sprint()
    {
        if (sprintWarning.activeSelf)
        {
            SprintWarning.SetActive(false);
        }
        //sprintColor.a += Time.deltaTime / 1.5f;
        //if (sprintColor.a >= 1)
        //{
        //    sprintColor.a = 1;
        //}
        //sprintImage.color = sprintColor;
        isSprinting = true;
        Camera.main.fieldOfView += Time.deltaTime * 20;
        if(Camera.main.fieldOfView > initialFOV + 15)
        {
            Camera.main.fieldOfView = initialFOV + 15;
        }
        currentStamina -= 20 * Time.deltaTime;
    }

    private void RegenerateLife()
    {
        if (life < maxRegenLife)
        {
            life += 3 * Time.deltaTime;

            if (life > maxRegenLife)
            {
                life = maxRegenLife;
            }
        }     
    }

    private void RegenerateStamina()
    {
        if (CooldownToRegen < MaxCooldownToRegenerate)
        {
            CooldownToRegen += Time.deltaTime;
            if (CooldownToRegen >= MaxCooldownToRegenerate)
            {
                currentStamina = maxStamina;
            }
        }
    }

    private void SlaughterDragon()
    {
        if (distanceFromDragon <= minDistanceFromDragon)
        {
            if (DragonSlaughtered == false)
            {
                slaughterText.SetActive(true);

                slaughterBar.gameObject.SetActive(false);

                if (Input.GetKeyDown(KeyCode.E) && Time.timeScale != 0)
                {
                    if (audioSrc.isPlaying == false)
                    {
                        audioSrc.clip = slaughterSound;
                        audioSrc.Play();
                    }
                }
                if (Input.GetKeyUp(KeyCode.E) && Time.timeScale != 0)
                {
                    if (audioSrc.isPlaying == true)
                    {
                        audioSrc.Stop();
                    }
                }
            }
            if (Input.GetKey(KeyCode.E) && Time.timeScale != 0)
            {
                if (DragonSlaughtered == false)
                {
                    slaughterText.SetActive(false);
                    slaughterBar.gameObject.SetActive(true);
                    slaughterTime += Time.deltaTime;
                    slaughterBar.size = slaughterTime / maxSlaughterTime;
                }
                
                if (slaughterTime >= maxSlaughterTime)
                {
                    
                    slaughterBar.gameObject.SetActive(false);
                    DragonSlaughtered = true;
                    //hasPuzzleArtifact = true;
                    Speed = equipedSpeed;
                    //CarUpgrades[0] = true;
                    slaughterTime = 0;
                    EnablePlayerEnemySpawner();

                }
              
            }
        }
        else
        {
            slaughterText.SetActive(false);
            slaughterBar.gameObject.SetActive(false);
        }
     }

    public void EnablePlayerEnemySpawner()
    {
        playerSpiderSpawnerRef.SetActive(true);
    }
    public void GetFlower(int flowerType)
    {
        if (flowerType == 0)
        {
            pulleyResistanceFlower++;
        }
        else if (flowerType == 1) 
        {
            resistanceFlower++;
        }
        else if (flowerType == 2)
        {
            shotgunFlower++;
        }
        else if(flowerType == 3)
        {
            succSpeedFlower++;
        }
    }


    private void CalculateDistance()
    {
       distanceFromDragon = Vector3.Distance(dragonTransform.transform.position, transform.position);
    }

}
