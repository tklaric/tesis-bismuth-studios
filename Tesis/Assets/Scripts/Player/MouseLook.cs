﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public float scopedSensitivity = 33;
    public float normalSensitivity = 100f;

    private float mouseSensitivityAux;
    private float scopedSenstivityAux;
    private float normalSensitivityAux;
    private float lowerClamp = -90f;
    private float higherClamp = 60f;
    private float xRotation = 0f;
    private Quaternion playerRotationAux;

    [SerializeField] Transform playerBody;
    [SerializeField] Transform initialTransform;
    [SerializeField] CrossbowShoot crossbowRef;
    [SerializeField] GameObject translucentFlower;
    [SerializeField] LayerMask ignoreLayer;

    private Transform myTarget;

    public Transform MyTarget { get => myTarget; }

    //NO SE VUELVE A ACOMODAR TODAVIA LA ROTACION DEL RIGIDBODY DESPUES DE SACARLE EL CONTROL AL PLAYER DEL MOUSELOOK.

    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        mouseSensitivityAux = mouseSensitivity;
        scopedSenstivityAux = scopedSensitivity;
        normalSensitivityAux = normalSensitivity;
        
    }
    public void pauseModeSensitivity()
    {
        mouseSensitivity = 0;
        scopedSensitivity = 0;
        normalSensitivity = 0;
    }
    public void resumeSensitivity()
    {
        mouseSensitivity = mouseSensitivityAux;
        scopedSensitivity = scopedSenstivityAux;
        normalSensitivity = normalSensitivityAux;
    }
    public void SaveCameraTransform()  //al empezar a apuntar
    {
        initialTransform.position = transform.position;
        initialTransform.rotation = transform.rotation;
        playerRotationAux = playerBody.rotation;
    }
    public void LoadCameraTransform() //al terminar
    {
        transform.position = initialTransform.position;
        transform.rotation = initialTransform.rotation;
        playerBody.rotation = playerRotationAux;
    }
    // Update is called once per frame
    void Update()
    {
        if(MyTarget != null)
        {
            transform.LookAt(MyTarget);
        }
        else
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;// * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;// * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, lowerClamp, higherClamp);

            transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
            playerBody.Rotate(Vector3.up * mouseX);

            TranslucentFlowerRayCast();
        }
        
    }
    
    public void TranslucentFlowerRayCast()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 2.5f, ~ignoreLayer))
        {

            if (hit.collider.gameObject.layer == 13)
            {
                translucentFlower.transform.forward += hit.point * Time.deltaTime;
                translucentFlower.transform.position = hit.point;
            }
        }
    }


    public void NormalMouseLook()
    {
        lowerClamp = -90f;
        higherClamp = 60f;
        xRotation = 0;
    }
    public void ScopedMouseLook()
    {
        lowerClamp = -90;
        higherClamp = -0f;
        xRotation = -15f;
    }
    
    public void ObtainTarget(Transform target)
    {
        myTarget = target;
    }
    public void ForgetTarget()
    {
        myTarget = null;
    }
    
}