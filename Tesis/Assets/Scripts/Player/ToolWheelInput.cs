﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolWheelInput : MonoBehaviour
{
    [SerializeField] UsableWheel usableWheel;
    [SerializeField] private KeyCode wheelKey = KeyCode.Tab;
    [SerializeField] FlowerSelector flowerSelector;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(wheelKey) && Time.timeScale != 0 && flowerSelector.FlowerButtonsActive == false)
        {
            //Enable Wheel Mode
            UsableWheel.instance.EnableWheel();
            UsableWheel.instance.CheckForCurrentUsable();
        }
        else if (Input.GetKeyUp(wheelKey))
        {
            //Disable Wheel Mode
            UsableWheel.instance.DisableWheel();
        }
    }
}
