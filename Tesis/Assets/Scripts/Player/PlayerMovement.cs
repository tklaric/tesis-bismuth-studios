﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    [SerializeField] float speed;
    [SerializeField] float sprintSpeed;
    [SerializeField] float gravity = -9.81f;
    [SerializeField] float distanceToGroundMax = 0.4f;
    [SerializeField] LayerMask whatIsGround;
    [SerializeField] Transform groundCheck;
    [SerializeField] List<AudioClip> playerFootSteps = new List<AudioClip>();
    [SerializeField] AudioClip sprintLoop;

    public Vector3 velocity;
    public Vector3 move;

    private bool coRoutineActive;
    private bool isMoving;
    private bool isOnGround;
    private Player player;
    private AudioSource audioSrc;

    public float Speed { get => speed; set => speed = value; }
    public float SprintSpeed { get => sprintSpeed; set => sprintSpeed = value; }

    private void Start()
    {
        player = GetComponentInChildren<Player>();
        controller = GetComponent<CharacterController>();
        audioSrc = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        isOnGround = Physics.CheckSphere(groundCheck.position, distanceToGroundMax, whatIsGround);

        if(isOnGround && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;

        controller.Move(move * Speed * Time.deltaTime);

        if (isMoving && coRoutineActive == false&&player.IsSprinting == false)
        {
            StartCoroutine(PlayWalkSounds());
            StopCoroutine(PlaySprintSounds());
        }

        

        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
        {            
            isMoving = false;
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            isMoving = true;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftShift) && isMoving && Time.timeScale != 0 && player.CurrentStamina > 0)
        {
            player.Sprint();
            player.IsSprinting = true;
            controller.Move(move * SprintSpeed * Time.deltaTime);
        }
        else
        {
            player.IsSprinting = false;
            player.NoSprinting();
            audioSrc.loop = false;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift)&&player.IsSprinting)
        {
            //StopCoroutine(PlayWalkSounds());
            //StartCoroutine(PlaySprintSounds());
            audioSrc.Stop();
            audioSrc.clip = sprintLoop;
            audioSrc.Play();
            audioSrc.loop = true;
        }
    }

    IEnumerator PlayWalkSounds()
    {
        coRoutineActive = true;

        if (isMoving)
        {
            int random = Random.Range(0, playerFootSteps.Count);
            audioSrc.clip = playerFootSteps[random];
            audioSrc.Play();
            yield return new WaitForSeconds(0.5f);
        }

        coRoutineActive = false;
    }

    IEnumerator PlaySprintSounds()
    {
        coRoutineActive = true;

        if (player.IsSprinting)
        {
            int random = Random.Range(0, playerFootSteps.Count);
            audioSrc.clip = playerFootSteps[random];
            audioSrc.Play();
            yield return new WaitForSeconds(0.4f);

        }
        coRoutineActive = false;
    }

}
