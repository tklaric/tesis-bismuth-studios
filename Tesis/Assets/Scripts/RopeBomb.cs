﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeBomb : MonoBehaviour
{
    [SerializeField] float damage;
    [SerializeField] GameObject bomb;
    [SerializeField] float radius;
    [SerializeField] GameObject explosionEffect;
    private bool hasHittedPlayer;
    [SerializeField] AudioSource audioSrc;
    BoxCollider mycollider;
    Renderer render;

    private void Start()
    {
        render = GetComponent<Renderer>();
        mycollider = GetComponent<BoxCollider>(); 
    }

    private void OnTriggerEnter(Collider other)
    {
        Detonate();
        audioSrc.Play();       
    }

    public void Detonate()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);       
        Vector3 explotionPosition = bomb.transform.position;
        hasHittedPlayer = false;
        Collider[] colliders = Physics.OverlapSphere(explotionPosition, radius);
        Player player = null;
        foreach (Collider hit in colliders)
        {
            player = hit.GetComponentInChildren<Player>();

            if (hasHittedPlayer == false && player != null)
            {
                player.PlayerTakeDamage(damage);                
                hasHittedPlayer = true;
            } 
            if(hit.gameObject.layer == 9)
            {                
                Spider spider = hit.gameObject.GetComponent<Spider>();
                if(spider != null)
                {
                    spider.TakeDamage(damage);
                }              
            }
        }
        mycollider.enabled = false;
        render.enabled = false;
        Destroy(gameObject, 1f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(bomb.transform.position,radius);
    }
}
