﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] // todas las clases que quieras que puedan guardar tienen que tener esto
public class SaveData
{
    private static SaveData _current;
    public static SaveData current
    {
        get // creo un singleton
        {
            if (_current==null)
            {
                _current = new SaveData();
            }
            return _current;
        }
        set
        {
            if (value != null)
            {
                _current = value;
            }
        }
    }

    public Vector3 _playerPosition;
    public Vector3 dragonPosition;
    public Vector3 _bloodSize;
    public int _succsCompleted;
    public float _actualBloodLitters;
    public float _higherBloodBar;
    public bool _hasPuzzleArtifact;
    public bool _dragonDied;

    public void SaveSuccEvent(int succsCompleted, float actualBloodLitters, bool IsPulleyBlocked,float higherBloodBar,Vector3 bloodSize,Vector3 playerPosition)
    {

        _succsCompleted = succsCompleted;
        _actualBloodLitters = actualBloodLitters;

        _higherBloodBar = higherBloodBar;
        _bloodSize = bloodSize;
        _playerPosition = playerPosition;

    } // agregar ocultamiento del mesh de la pulley

    public void SaveAltarEvent(bool hasPuzzleArtifact, Vector3 playerPosition)
    {
        _hasPuzzleArtifact = hasPuzzleArtifact;
        _playerPosition = playerPosition;
    }

    public void SaveDragonDeath(bool DragonDeath, Vector3 drakePos)
    {
        _dragonDied = DragonDeath;
        dragonPosition = drakePos;
    }

}
