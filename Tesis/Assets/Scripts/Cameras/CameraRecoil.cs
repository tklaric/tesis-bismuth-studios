﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRecoil : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 6;
    [SerializeField] float returnSpeed = 25;
    public float adjustableRecoilCrossbow = 1;
    public float adjustableRecoilShotgun;

    [SerializeField] Vector3 recoilRotation = new Vector3(2f, 2f, 2f);

    Vector3 currentRotation;
    Vector3 rot;


    private void FixedUpdate()
    {
        currentRotation = Vector3.Lerp(currentRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        rot = Vector3.Slerp(rot, currentRotation, rotationSpeed * Time.fixedDeltaTime);
        transform.localRotation = Quaternion.Euler(rot);
    }

   

    public void RecoilCrossbow()
    {
        currentRotation += new Vector3(-recoilRotation.x, Random.Range(-recoilRotation.y, recoilRotation.y)/adjustableRecoilCrossbow, Random.Range(-recoilRotation.z, recoilRotation.z)/adjustableRecoilCrossbow);
    }
    public void RecoilShotgun()
    {
        currentRotation += new Vector3(-recoilRotation.x, Random.Range(-recoilRotation.y, recoilRotation.y) / adjustableRecoilShotgun, Random.Range(-recoilRotation.z, recoilRotation.z) / adjustableRecoilShotgun);

    }

}
