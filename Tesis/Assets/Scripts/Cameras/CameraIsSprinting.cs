﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraIsSprinting : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] Player player;
    [SerializeField] Animator camAnim;

    private void Awake()
    {
        Player player = GetComponent<Player>();
    }

    private void Update()
    {
        PlayerSprinting();
    }

    public void PlayerSprinting()
    {
        if(player.IsSprinting == true)
        {
            camAnim.SetBool("Sprint", true);
            cam.fieldOfView++;
            if(cam.fieldOfView >= 70)
            {
                cam.fieldOfView = 70;
            }
        }
        if (player.IsSprinting == false)
        {
            camAnim.SetBool("Sprint", false);
            cam.fieldOfView--;
            if(cam.fieldOfView <= 60)
            {
                cam.fieldOfView = 60;
            }
        }
        
    }
}
