﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerM4Bullet : MonoBehaviour
{
    public float damage;
    private Spider spider;
    private void Awake()
    {
        Destroy(this.gameObject, 2f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Earth Enemy") //convendria poner un tag a los enemigos terrestres y chekear eso
        {
            spider = collision.gameObject.GetComponent<Spider>();
            spider.TakeDamage(damage);
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }
}
