﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingZone : MonoBehaviour
{
    [SerializeField] CrossbowShoot weapon;
    [SerializeField] Player player;

    private void OnTriggerEnter(Collider other)
    {
        player = other.GetComponent<Player>();
        //weapon.Canshoot = true;
    }

    private void OnTriggerExit(Collider other)
    {
        player = other.GetComponent<Player>();
       // weapon.Canshoot = false;
    }
}
