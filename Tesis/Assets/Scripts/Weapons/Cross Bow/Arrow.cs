﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    TrailRenderer trail;
    Pulleys pulley;
    public Rigidbody rb;
    WindArea wind;
    [SerializeField] Material chainMat;
    [SerializeField] Material cableMat;
    private Transform myTarget;
    private Vector3 velocity;
    

    bool hasHittedDragon;
    

    public bool HasHittedDragon { get => hasHittedDragon; set => hasHittedDragon = value; }
    public Pulleys Pulley { get => pulley; set => pulley = value; }
    public Transform MyTarget { get => myTarget; set => myTarget = value; }
    public Vector3 Velocity { get => velocity; set => velocity = value; }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        wind = GetComponent<WindArea>();
        trail = GetComponent<TrailRenderer>();
    }
    public void UseChainMat()
    {
        trail.material = chainMat;
    }
    public void UseCableMat()
    {
        trail.material = cableMat;
    }
    private void Update()
    {
        rb.AddForce(wind.Direction * wind.Strength);
        if(!HasHittedDragon)
        {
            Seek(myTarget.position, 200, 1000);
            
            transform.position += Velocity * Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == 11)
        {
            hasHittedDragon = true;
            TargetHitted();
            gameObject.transform.position = collision.collider.gameObject.transform.position;
            gameObject.transform.localPosition += Vector3.back/1f;
            gameObject.transform.parent = collision.collider.gameObject.transform;
            pulley.SetChainEnd(gameObject.transform);
            rb.isKinematic = true;
            trail.enabled = false;
        } 
    }
    private void Start()
    {
        velocity += Vector3.forward * 3;
    }
    public void TargetHitted()
    {
        pulley.Crossbow.ScopeOverlay.SetActive(false);
        pulley.Crossbow.WeaponCamera.SetActive(true);
        pulley.Crossbow.Player.IsAiming = false;
        pulley.Crossbow.Unscoped();
        pulley.Crossbow.Player.playerMovement.enabled = true;
        pulley.Crossbow.MouseLookReference.NormalMouseLook();
        pulley.Crossbow.Player.inputWheelReference.enabled = true;
        pulley.UseHarpoonText.SetActive(false);
        pulley.Crossbow.AimMinigamesRef.enabled = false;
    }
    public void Seek(Vector3 targetPosition, float maxSpeed, float steeringSpeed)
    {
        Vector3 diff = targetPosition - transform.position;
        Vector3 dir = Vector3.Normalize(diff);
        Vector3 desiredVelocity = dir * maxSpeed;

        Vector3 velDiff = desiredVelocity - Velocity;
        Vector3 velDir = Vector3.Normalize(velDiff);
        Vector3 steeringForce = velDir * steeringSpeed;


        Velocity += steeringForce * Time.deltaTime;
    }
}
