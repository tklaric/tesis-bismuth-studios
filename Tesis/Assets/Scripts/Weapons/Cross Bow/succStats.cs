﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class succStats : MonoBehaviour
{
    //succ speed, line strenght, etc.
    public float lineHp;
    public float lineStrengh;
    public float lineSpeed;
    private float maxLineHp;

    public float MaxLineHp { get => maxLineHp; set => maxLineHp = value; }

    private void Awake()
    {
        MaxLineHp = lineHp;
    }

}
