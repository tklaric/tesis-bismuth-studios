﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CrossbowShoot : MonoBehaviour
{
    [SerializeField] GameObject arrowPrefab;
    [SerializeField] GameObject text;
    [SerializeField] GameObject scopeOverlay;
    [SerializeField] GameObject weaponCamera;
    [SerializeField] GameObject shootcollider;
    [SerializeField] Transform arrowLocation;
    [SerializeField] CameraRecoil cameraRecoil;
    [SerializeField] MouseLook mouseLookReference;
    [SerializeField] AimMinigames aimMinigamesRef;
    [SerializeField] LayerMask layer;
    [SerializeField] AudioSource shootSound;
    [SerializeField] Player player;
    [SerializeField] Camera mainCamera;
    [SerializeField] TextMeshProUGUI missText;
    [SerializeField] bool isLethal;
    [SerializeField] bool isSyringe;
  

    public succStats succStatsRef;

    private float shotPower = 5000f;
    private float scopedFOV = 10f;
    private float shootCoolDown = 3f;
    private float normalFOV;
    private float maxZoom = 10f;
    private float minZoom = 60f;
    private bool isInteractable = false;
    private bool canshoot;


    private Pulleys pulleyReference;        
    private WindArea wind;
    private RaycastHit hit;
    private Color32 missColor = new Color32(219, 150, 150, 255);


    public bool Canshoot { get => canshoot; set => canshoot = value; }
    public bool IsLethal { get => isLethal; set => isLethal = value; }
    public bool IsSyringe { get => isSyringe; set => isSyringe = value; }
    public Pulleys PulleyReference { get => pulleyReference; set => pulleyReference = value; }
   // public GameObject UseHarpoonText { get => useHarpoonText; set => useHarpoonText = value; }
    public GameObject Shootcollider { get => shootcollider; set => shootcollider = value; }
    public GameObject ScopeOverlay { get => scopeOverlay; set => scopeOverlay = value; }
    public GameObject WeaponCamera { get => weaponCamera; set => weaponCamera = value; }
    public Player Player { get => Player1; set => Player1 = value; }
    public Player Player1 { get => player; set => player = value; }
    public MouseLook MouseLookReference { get => mouseLookReference; set => mouseLookReference = value; }
    public Transform ArrowLocation { get => arrowLocation; }
    public AimMinigames AimMinigamesRef { get => aimMinigamesRef; set => aimMinigamesRef = value; }

    void Start()
    {
        wind = GetComponentInParent<WindArea>();
        normalFOV = mainCamera.fieldOfView;

        
    }
    private void OnTriggerEnter(Collider other)
    {
        isInteractable = true;
    }
    

    private void OnTriggerExit(Collider other)
    {
        isInteractable = false;
    }
    private void OnDisable()
    {
        isInteractable = false;
    }
    
   
    void Update()
    {
        
        if (isInteractable)
        {
            ScopingAndUnscoping();
        }
        Shoot();
        //Aim();

        if (missText.enabled == true)
        {
            missText.color = missColor;
            missColor.a -= 1;

            if (missColor.a <= 0)
            {
                missText.enabled = false;
                missColor.a = 255;
            }
        }

    }


    void Shoot()
    {
        
        if (Player.IsAiming == true)
        {
            shootCoolDown += Time.deltaTime; //no en uso
            if (Input.GetMouseButtonDown(0) && AimMinigamesRef.allGamesCompleted && shootCoolDown > 6)
            {
                
                text.SetActive(false);
                GameObject arrowGO;
                arrowGO = Instantiate(arrowPrefab, ArrowLocation.position, ArrowLocation.rotation);
                shootSound.Play();
                Arrow arrowComponent = arrowGO.GetComponent<Arrow>();

                if (AimMinigamesRef.ProbabilitySuccess)
                {
                    arrowComponent.MyTarget = mouseLookReference.MyTarget;
                }
                else
                {
                    Invoke("ActivateMissText", 1.5f);
                    int randomTarget =  Random.Range(0, 4);
                    arrowComponent.MyTarget = mouseLookReference.MyTarget.GetComponentInParent<Dragon>().MyMissPoints[randomTarget];
                }
                
                arrowComponent.Pulley = pulleyReference;
                //arrowComponent.rb.AddRelativeForce(Vector3.forward * shotPower); //REEMPLAZAR con seek(4 partes del cuerpo en orden)
                if(IsLethal)
                {
                    arrowComponent.UseChainMat();
                }
                else
                {
                    arrowComponent.UseCableMat();
                }
                mouseLookReference.ForgetTarget();
                cameraRecoil.RecoilCrossbow();
                shootCoolDown = 0;

            }
            
        }
        
    }

    private void ActivateMissText()
    {
        missText.enabled = true;
    }

    private void ScopingAndUnscoping()
    {
        if (pulleyReference.HasChainConnected == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (Player.IsAiming == false)
                {
                    mouseLookReference.SaveCameraTransform();
                    Player.IsAiming = true;
                    pulleyReference.UseHarpoonText.SetActive(false);
                    ScopeOverlay.SetActive(true);
                    WeaponCamera.SetActive(false);
                    Player.playerMovement.enabled = false;
                    Player.inputWheelReference.enabled = false;
                    UsableSwitcher.instance.DeactivateCurrentTool();
                    MouseLookReference.ScopedMouseLook();
                    AimMinigamesRef.enabled = true;
                }
                else
                {
                    mouseLookReference.LoadCameraTransform();
                    Player.IsAiming = false;
                    pulleyReference.UseHarpoonText.SetActive(true);
                    ScopeOverlay.SetActive(false);
                    WeaponCamera.SetActive(true);
                    Unscoped();
                    Player.playerMovement.enabled = true;
                    Player.inputWheelReference.enabled = true;
                    MouseLookReference.NormalMouseLook();
                    AimMinigamesRef.enabled = false;
                }
            }
        }
       

        if (Player.IsAiming)
        {
            Scoped();
        }
    }
    public void Aim()
    {
        //if (Input.GetButton("Fire2"))
        //{

        Player.IsAiming = true;
        //anim.SetBool("IsAiming", true);
        Scoped();
            //Invoke("Scoped", 0.15f);
        //}
        //else
        //{
        //    p.IsAiming = false;
        //    anim.SetBool("IsAiming", false);
        //    Invoke("Unscoped", 0.08f);
        //}
    }

    void Scoped()
    {
        mouseLookReference.mouseSensitivity = mouseLookReference.scopedSensitivity;
        pulleyReference.HideMesh();
        if(Input.GetAxis("Mouse ScrollWheel") > 0f && Time.timeScale != 0)
        {
            mainCamera.fieldOfView -= scopedFOV;

            if (mainCamera.fieldOfView <= maxZoom)
            {
                mainCamera.fieldOfView = maxZoom;
                cameraRecoil.adjustableRecoilCrossbow = 10;
            }
            else
            {
                cameraRecoil.adjustableRecoilCrossbow+=2;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f && Time.timeScale != 0)
        {
            mainCamera.fieldOfView += scopedFOV;

            if (mainCamera.fieldOfView >= minZoom)
            {
                mainCamera.fieldOfView = minZoom;
                cameraRecoil.adjustableRecoilCrossbow = 1;
            }
            else
            {
                cameraRecoil.adjustableRecoilCrossbow-=2;
            }           
        }

    }

    public void Unscoped()
    {
        mouseLookReference.mouseSensitivity = mouseLookReference.normalSensitivity;
        mainCamera.fieldOfView = normalFOV;
        pulleyReference.ShowMesh();
    }             
}
