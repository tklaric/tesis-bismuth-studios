﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    int selectedWeapon = 0;
    [SerializeField]AirSonar sonar;
    [SerializeField] GameObject bulletText;
    [SerializeField] List<GameObject> weapons = new List<GameObject>();
    [SerializeField]ShotgunShoot shotgun;
    [SerializeField] GameObject crossBow;
    int actualWeapon;

    void Update()
    {
        int previousSelectedWeapon = selectedWeapon;
        AlreadyEquiped();

        if (Input.GetKeyDown(KeyCode.Alpha1) && shotgun.IsAnimating == false)
        {
            actualWeapon = 0;
            bulletText.SetActive(true);
            weapons[0].SetActive(true);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && shotgun.IsAnimating == false)
        {
            actualWeapon = 1;
            bulletText.SetActive(false);
            weapons[0].SetActive(false);
            weapons[1].SetActive(true);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);

        }
        else if (Input.GetKey(KeyCode.Alpha3) && shotgun.IsAnimating == false)
        {
            actualWeapon = 2;
            bulletText.SetActive(false);
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(true);
            weapons[3].SetActive(false);

        }
        else if (Input.GetKey(KeyCode.Alpha4) && shotgun.IsAnimating == false)
        {
            actualWeapon = 3;
            bulletText.SetActive(false);
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(true);

        }
        if (crossBow.activeSelf == true)
        {
            for (int i = 0; i < weapons.Count; i++)
            {
                weapons[i].SetActive(false);
            }
        }
    }

    public void AlreadyEquiped()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && weapons[0] == true && shotgun.IsAnimating == false)
        {
            bulletText.SetActive(false);
            weapons[0].SetActive(false);
            weapons[1].SetActive(true);
        }
    }
}
