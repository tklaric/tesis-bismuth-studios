﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShotgunShoot : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] int pelletNumber;
    [SerializeField] int maxAmmo;
    [SerializeField] float coneSize;
    [SerializeField] ParticleSystem muzzleFlash;
    [SerializeField] GameObject hitEffect;
    [SerializeField] GameObject waterEffect;
    [SerializeField] GameObject waterAudioEffect;
    [SerializeField] GameObject pulleyAudioEffect;
    [SerializeField] GameObject greenBloodHitEffect;
    [SerializeField] TextMeshProUGUI buffTimerText;
    [SerializeField] Transform bulletLocation;
    [SerializeField] AudioSource bulletsImpactAudioSource;
    [SerializeField] AudioClip audioShoot;
    [SerializeField] AudioClip audioRecharge;
    [SerializeField] AudioClip audioBulletLoaded;
    
    [SerializeField] float animatorShootSpeed = 3;
    [SerializeField] float animatorRechargeSpeed = 1;
    [SerializeField] Animator shotgunAnimator;
    [SerializeField] GameObject VanishUI;
    [SerializeField] GameObject crosshair;
    [SerializeField] Text bulletsText;
    [SerializeField] LayerMask layers;
    [SerializeField] CameraRecoil cameraRecoilRef;
    [SerializeField] float timePerBulletLoaded = 0.2f;
    
    int currentAmmo;
    float maxAmmoAux; //para usar en una division sin usar INTs
    float currentAmmoAux;

    bool speedIsBuffed;
    bool isAnimating;
    bool recharging;
    bool hittedWater;
    bool hittedPulley;
    bool hittedRope;

    float shootingCooldown;
    float timeToReload;

    AudioSource shotgunAudioSource;
    

    public bool IsAnimating { get => isAnimating; set => isAnimating = value; }
    public bool SpeedIsBuffed { get => speedIsBuffed; set => speedIsBuffed = value; }
    public TextMeshProUGUI BuffTimerText { get => buffTimerText; set => buffTimerText = value; }

    private void Start()
    {
        IsAnimating = false;
        shotgunAudioSource = GetComponent<AudioSource>();
        recharging = false;
        shootingCooldown = 0;
        timeToReload = 0;
        currentAmmo = maxAmmo;
        maxAmmoAux = maxAmmo;
        bulletsText.text = currentAmmo.ToString(currentAmmo + " / " + maxAmmo);
    }

    void Update()
    {
        float minigameFloat2 = Mathf.Sin(Time.time * 1);

        Use();
        if (Input.GetKeyDown(KeyCode.R) && Time.timeScale != 0)
        {
            if (currentAmmo != maxAmmo && !recharging)
            {
                Recharge();
            }
        }
    }

    public void Use()
    {
        if(SpeedIsBuffed)
        {
            shootingCooldown += Time.deltaTime * 1.50f;
        }
        else
        {
            shootingCooldown += Time.deltaTime;
        }
        

        if (Input.GetMouseButtonDown(0) && shootingCooldown >= 1 && recharging == false && Time.timeScale != 0)
        {
            if(currentAmmo == 0)
            {
                Recharge();
                return;
            }
            hittedPulley = false;
            hittedWater = false;
            hittedRope = false;
            cameraRecoilRef.RecoilShotgun();
            IsAnimating = true;
            shotgunAnimator.speed = animatorShootSpeed;
            shotgunAnimator.SetTrigger("Shoot");
            shotgunAudioSource.clip = audioShoot;
            shotgunAudioSource.Play();
            muzzleFlash.Play();
            currentAmmo--;
            bulletsText.text = currentAmmo.ToString(currentAmmo + " / " + maxAmmo);
            shootingCooldown = 0;
            RaycastHit hit;
            bool already_instanced = false;
            for (int i = 0; i < pelletNumber; i++)
            {
                Vector3 spread = Vector3.zero;
                Vector3 direction = bulletLocation.forward;
                spread += bulletLocation.up * Random.Range(-1f, 1f) * coneSize;
                spread += bulletLocation.right * Random.Range(-1f, 1f) * coneSize;

                direction += spread.normalized * Random.Range(0f, 0.1f);


                if (Physics.Raycast(bulletLocation.transform.position, direction, out hit, 500, layers))
                {
                    if (hit.collider.gameObject.layer == 9)
                    {

                        IGroundEnemy groundEnemy = hit.transform.gameObject.GetComponent<IGroundEnemy>();
                        groundEnemy.TakeDamage(damage);
                        if (!already_instanced)
                        {
                            already_instanced = true;
                            GameObject impactGO = Instantiate(greenBloodHitEffect, hit.point, Quaternion.LookRotation(Vector3.zero));
                            Destroy(impactGO, 2f);
                        }
                    }
                    else if (hit.collider.gameObject.layer == 13 || hit.collider.gameObject.layer == 23)
                    {
                        GameObject impactGO = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                        Destroy(impactGO, 2f);
                    }
                    else if(hit.collider.gameObject.layer == 24)
                    {
                        GameObject impactGO = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                        Destroy(impactGO, 2f);
                        if(!hittedPulley)
                        {
                            hittedPulley = true;
                            GameObject impactSoundGO = Instantiate(pulleyAudioEffect, hit.point, Quaternion.identity);
                            Destroy(impactSoundGO,1f);
                        }
                    }
                    else if(hit.collider.gameObject.layer == 4)
                    {
                        GameObject impactGO = Instantiate(waterEffect, hit.point, Quaternion.LookRotation(hit.normal));
                        Destroy(impactGO, 2f);
                        
                        if(!hittedWater)
                        {
                            hittedWater = true;
                            GameObject impactSoundGO = Instantiate(waterAudioEffect, hit.point, Quaternion.identity);
                            Destroy(impactSoundGO,2);
                        }
                    }
                    else if(hit.collider.gameObject.layer == 22)
                    {
                        if(!hittedRope)
                        {
                            hittedRope = true;
                            hit.collider.gameObject.GetComponent<RopeBomb>().Detonate();
                        }
                    }
                }
                else
                {
                    //si entraste aca es xq le dio null al raycast osea le disparaste al cielo o pusiste la escopeta bajo tierra
                }
            }
            hittedWater = false;
            hittedPulley = false;
        }
        
    }

    void Recharge()
    {
        
        currentAmmoAux = currentAmmo;
        shotgunAnimator.speed = 0.7f + (currentAmmoAux / 10);
        
        shotgunAnimator.SetBool("Recharge", true);
        recharging = true;
        IsAnimating = true;
        shotgunAudioSource.clip = audioRecharge;
        shotgunAudioSource.Play();
        Invoke("LoadBullet", timePerBulletLoaded * 2);
        
    }
    void LoadBullet()
    {
        shotgunAudioSource.clip = audioBulletLoaded;
        shotgunAudioSource.Play();
        currentAmmo++;
        bulletsText.text = currentAmmo.ToString(currentAmmo + " / " + maxAmmo);
        if (currentAmmo<maxAmmo)
        {
            Invoke("LoadBullet", timePerBulletLoaded);
        }
    }

    

    void AmmoRecharged()
    {
        shotgunAnimator.SetBool("Recharge", false);
        //currentAmmo = maxAmmo;
        recharging = false;
        IsAnimating = false;
        bulletsText.text = currentAmmo.ToString(currentAmmo + " / " + maxAmmo);
    }

    public void OnEnable()
    {
        VanishUI.SetActive(true);
        crosshair.SetActive(true);
        recharging = false;
    }

    public void OnDisable()
    {
        CancelInvoke("LoadBullet");
        crosshair.SetActive(false);
        VanishUI.SetActive(false);
    }
}
