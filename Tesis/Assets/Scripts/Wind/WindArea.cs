﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindArea : MonoBehaviour
{
    [SerializeField] float strength;
    [SerializeField] Vector3 direction;

    public float Strength { get => strength; set => strength = value; }
    public Vector3 Direction { get => direction; set => direction = value; }
}
