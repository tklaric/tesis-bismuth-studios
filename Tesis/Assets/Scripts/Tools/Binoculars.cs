﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Binoculars : MonoBehaviour
{
    [SerializeField] GameObject binocularsOverlay;
    [SerializeField] GameObject weaponCamera;
    [SerializeField] Animator anim;
    [SerializeField] LayerMask layer;
    [SerializeField] Camera mainCamera;
    [SerializeField] Text distanceFromObjectText;
    [SerializeField] Post_Scoupe pp;
    [SerializeField] Camera cam;

    private ToolWheelInput wheelInput;
    private MouseLook mouseLook;

    private RaycastHit hit;
    private Player p;

    float scopedFOV = 10f;
    float normalFOV;
    float maxZoom = 10f;
    float minZoom = 60f;

    int distanceFromObject;

    
    void Start()
    {
        p = GetComponentInParent<Player>();
        normalFOV = mainCamera.fieldOfView;
        wheelInput = GetComponentInParent<ToolWheelInput>();
        mouseLook = GetComponentInParent<MouseLook>();
    }

    void Update()
    {
        Aim();
    }

    void Aim()
    {
        if (Input.GetMouseButtonDown(1) && Time.timeScale != 0)
        {
            Use();                       
        }        
        else if(Input.GetMouseButtonUp(1) && Time.timeScale != 0)
        {                     
            anim.SetBool("isScoped", false);
            Invoke("Unscoped", 0.45f);
        }
    }

    //IEnumerator WaitForPP()
    //{
    //    yield return new WaitForSeconds(2);
    //    Use();
    //}

    public void Use()
    {       
        p.IsZoom = true;
        UsableWheel.instance.DisableWheel();
        wheelInput.enabled = false;
        anim.SetBool("isScoped", true);
        Invoke("Scoped", 0.45f);
    }

    void Scoped()
    {
        cam.cullingMask |= 1 << LayerMask.NameToLayer("Rope");
        pp.enabled = true;
        binocularsOverlay.SetActive(true);
        distanceFromObjectText.gameObject.SetActive(true);
        weaponCamera.SetActive(false);
        CalculateDistance();

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            mainCamera.fieldOfView -= scopedFOV;

            if (mainCamera.fieldOfView <= maxZoom) mainCamera.fieldOfView = maxZoom;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            mainCamera.fieldOfView += scopedFOV;

            if (mainCamera.fieldOfView >= minZoom) mainCamera.fieldOfView = minZoom;
        }

    }

    void Unscoped()
    {
        p.IsZoom = false;
        wheelInput.enabled = true;
        cam.cullingMask &= ~(1 << LayerMask.NameToLayer("Rope"));
        pp.enabled = false;
        binocularsOverlay.SetActive(false);
        distanceFromObjectText.gameObject.SetActive(false);
        weaponCamera.SetActive(true);

        mainCamera.fieldOfView = normalFOV;
    }

    void CalculateDistance()
    {
        if (Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity,layer, QueryTriggerInteraction.Collide))
        {
            distanceFromObject = (int)hit.distance;
            distanceFromObjectText.text = distanceFromObject.ToString();
        }
    }

}
