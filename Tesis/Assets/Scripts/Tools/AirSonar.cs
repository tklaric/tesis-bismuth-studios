﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    public class AirSonar : MonoBehaviour
    {
        [SerializeField] Transform dragonPosition;
        [SerializeField] Text sonarData;

        float distanceFromDragon;

    

        private void Update()
        {
            ActivateSonar();      
        }

        void ActivateSonar()
        {
            distanceFromDragon = Vector3.Distance(dragonPosition.transform.position, transform.position);
            sonarData.text = "Distance:" + ((int)distanceFromDragon).ToString();
        }


    }
