﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulleyFinder : MonoBehaviour
{
    //esta script va a estar attacheada al modelo del soanr
    //va a tener un hijo text attacheado pero los cambios en el texto se dicen aca
    [SerializeField] PointsOfInterest pointsOfInterest;
    [SerializeField] AudioSource beepSound;
    [SerializeField] Text distanceText;
    [SerializeField] Text targetText;

    private Transform selectedTransform;
    private int index;
    private int totalAmmountOfInterestPoints;
    private float distance;
    private float soundTimer;
    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        SelectPulley();
        //las pulleys tienen q estar a partir de ahora arriba de estos puntos de interes ya que se mide la distancia a eso.
        totalAmmountOfInterestPoints = pointsOfInterest.transformList.Count - 1;
        distance = Vector3.Distance(selectedTransform.position, transform.position);
        distanceText.text = "DISTANCE: " + ((int)distance).ToString() + "m";
        targetText.text = "TARGET: " + (index + 1);

    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(selectedTransform.transform.position, transform.position);
        Use();
        PlayBeepSound();
        
        distanceText.text = "DISTANCE: " + ((int)distance).ToString() + "m";
    }

    void PlayBeepSound()
    {
        soundTimer += Time.deltaTime;
        if (soundTimer>=3f)
        {
            beepSound.Play();
            soundTimer = 0;
        }
        
    }

    void SelectPulley()
    {
        selectedTransform = pointsOfInterest.transformList[index];
    }

    public void Use()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (index != totalAmmountOfInterestPoints)
            {
                index++;
                SelectPulley();
                targetText.text = "TARGET: " + (index + 1);
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            if (index != 0)
            {
                index--;
                SelectPulley();
                targetText.text = "TARGET: " + (index + 1);
            }
        }
    }
}
