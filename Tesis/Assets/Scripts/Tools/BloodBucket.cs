﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodBucket : MonoBehaviour
{
    public float maxBloodLiters;//varia segun tamaño del frasco
    public float actualBloodLiters;
    private Pulleys nearPulley;
    private Pulleys pulleyImConnectedTo;
    [SerializeField] Transform positionInPlayerHand;
    private SphereCollider myCollider;


    public bool hasAncientBlood;
    public bool isInPosition;
    [SerializeField] private GameObject altarReference;
    [SerializeField] private GameObject bloodFill;
    [SerializeField] Color bloodColor1;
    [SerializeField] Color bloodColor2;
    Color lerpedColor;
    [SerializeField] GameObject bloodInsideJar;
    Renderer bloodInJarRenderer;

    public Pulleys NearPulley { get => nearPulley; set => nearPulley = value; }
    public GameObject BloodFill { get => bloodFill; set => bloodFill = value; }


    //Cuando se conecta el bloodbucket a la pulley deberia aparecer en la UI el bucket con su respectivo contenido y % de llenado o cantidad de litros

    private void Awake()
    {
        myCollider = GetComponent<SphereCollider>();
        isInPosition = false;
        hasAncientBlood = false;
        bloodInJarRenderer = bloodInsideJar.GetComponent<Renderer>();

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0)
        {
            if(NearPulley != null && hasAncientBlood == false)
            {
                Debug.Log(NearPulley.gameObject);
                ConnectToPulley();
                bloodInJarRenderer.material.SetColor("_Color", bloodColor1);
            }
        }
        if(myCollider.enabled == true) //osea NO estoy conectado a la pulley, xq si lo estoy seguir cambiando los colores es un desperdicio
        {
            float distance = (Vector3.Distance(transform.position, altarReference.transform.position)) / 33; //a mayor valor mayor area de sensibilidad
            lerpedColor = Color.Lerp(bloodColor2, bloodColor1, distance);
            bloodInJarRenderer.material.SetColor("_Color", lerpedColor);
        }

    }

    private void OnTriggerEnter(Collider other)
    {        
        nearPulley = other.GetComponent<Pulleys>();
    }
    private void OnTriggerExit(Collider other)
    {
        nearPulley = null;
    }

    void ConnectToPulley()
    {
        NearPulley.JarAlreadyConnect = true;
        NearPulley.PreBucket1.SetActive(false);
        myCollider.enabled = false;
        pulleyImConnectedTo = NearPulley;
        pulleyImConnectedTo.BloodBucketConnected = this;
        this.gameObject.transform.parent = pulleyImConnectedTo.transform;
        this.gameObject.transform.position = pulleyImConnectedTo.BloodJarPosition.position;
        this.gameObject.transform.rotation = pulleyImConnectedTo.BloodJarPosition.rotation;
    }
    public void DisconnectFromPulley()
    {
        //se rompe todo xq se entra multiples veces ca xq hay muchas arañitas tataacndo la pulley y la matan 50 veces y solo se puede matar 1 vez a la pilley no 150.
        //cuando se rompa todo poner algun lugar q se reseteen las stats pertinentes a una pulley nuevita.
        NearPulley.JarAlreadyConnect = false;
        NearPulley.PreBucket1.SetActive(true);       
        myCollider.enabled = true;
        pulleyImConnectedTo.BloodBucketConnected = null;
        pulleyImConnectedTo.Crossbow.succStatsRef.lineHp = pulleyImConnectedTo.Crossbow.succStatsRef.MaxLineHp; //seteo la vida de vuelta antes de nullearla.
        pulleyImConnectedTo = null;
        MoveToPlayerHand();
    }

    public void EmptyBlood()
    {
        hasAncientBlood = false;
        BloodFill.SetActive(false);
    }

    public void FillBlood()
    {
        hasAncientBlood = true;
        BloodFill.SetActive(true);
    }

    public void MoveToPlayerHand()
    {
        this.gameObject.transform.parent = positionInPlayerHand.transform;
        this.gameObject.transform.position = positionInPlayerHand.position;
        this.gameObject.transform.rotation = positionInPlayerHand.rotation;
    }

    public void Use()
    {
        if (hasAncientBlood)
        {
            // descometnaqr despues de la entrega a nico
            if (isInPosition)
            {
                altarReference.GetComponent<AltarRaise>().enabled = true;
                //EmptyBlood();
                bloodFill.SetActive(false);
            }          
        }
    }
}