﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Diary : Usable
{

    [SerializeField] GameObject diaryCanvas;

    private ToolWheelInput wheelInput;
    private Animator anim;
    private MouseLook mouseLook;
    private float timer;

    private bool bookCanvasIsActive;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        mouseLook = GetComponentInParent<MouseLook>();
        wheelInput = GetComponentInParent<ToolWheelInput>();
    }

    private void Update()
    {
        OpenDiary();

        if (bookCanvasIsActive == true)
        {
            UsableWheel.instance.DisableWheel();
            wheelInput.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            mouseLook.enabled = false;
            
        }
        else
        {         
            wheelInput.enabled = true;
        }

    }

    void OpenDiary()
    {
        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0)
        {
            
            anim.SetBool("Open", true);
            anim.SetBool("Close", false);
            bookCanvasIsActive = true;
        }
        if (Input.GetMouseButtonDown(1) && Time.timeScale != 0)
        {
            diaryCanvas.SetActive(false);
            bookCanvasIsActive = false;
            anim.SetBool("Close", true);
            anim.SetBool("Open", false);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            mouseLook.enabled = true;

        }
    }

    public void ActivateDiaryUI() 
    {
        diaryCanvas.SetActive(true);
    }
}
