﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ReduceColorAlpha : MonoBehaviour
{
    private TextMeshProUGUI completedFailedText;
    private Color32 succesColor = new Color32(106, 202, 156, 255);
    private Color32 failColor = new Color32(197, 116, 96, 255);

    private void Start()
    {
        completedFailedText = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        if (completedFailedText.enabled == true)
        {
            completedFailedText.alpha -= 0.15f * Time.deltaTime;

            if (completedFailedText.alpha<=0)
            {
                completedFailedText.alpha = 1;
                completedFailedText.enabled = false;
            }
        }
    }



}
