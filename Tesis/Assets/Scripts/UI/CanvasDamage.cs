﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasDamage : MonoBehaviour
{
    [SerializeField] Animator bloodanim;

    public void Bleeding()
    {
        bloodanim.SetTrigger("Damaging");
    }
}
