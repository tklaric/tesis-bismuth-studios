﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodCanvas : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] GameObject[] bloodCanvas;

    private void Update()
    {
        ActivateCanvas();
    }

    public void ActivateCanvas()
    {
        if(player.Life <= 80 && player.Life >= 40)
        {
            bloodCanvas[0].SetActive(true);
            bloodCanvas[1].SetActive(false);
        }

        else if(player.Life <= 40 && player.Life >= 0)
        {
            bloodCanvas[0].SetActive(false);
            bloodCanvas[1].SetActive(true);
        }

        else
        {
            bloodCanvas[0].SetActive(false);
            bloodCanvas[1].SetActive(false);
        }
    }
}
