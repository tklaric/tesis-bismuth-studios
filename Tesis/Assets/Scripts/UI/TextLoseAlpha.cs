﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextLoseAlpha : MonoBehaviour
{
    private Text text;
    private Color auxColor;
    private Color originalColor;
    private float counter;
    [SerializeField] float timeUntilFadeOutStarts;
    [SerializeField] float fadeOutStrenght;

    private void Awake()
    {
        text = GetComponent<Text>();
        auxColor = text.color;
        originalColor = text.color;
        counter = 0;
    }
    private void Update()
    {
        if(text.color.a <=0)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            counter += Time.deltaTime;
            if(counter >= timeUntilFadeOutStarts)
            {
                auxColor.a -= fadeOutStrenght * Time.deltaTime;
                text.color = auxColor;
            }
        }
    }
    private void OnEnable()
    {
        counter = 0;
        auxColor = originalColor;
        text.color = originalColor;
    }
}
