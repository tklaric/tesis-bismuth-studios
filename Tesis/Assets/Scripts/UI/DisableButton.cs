﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableButton : MonoBehaviour
{
    [SerializeField] Button button;
    [SerializeField] Button zone2button;
    [SerializeField] Player player;
    private bool _isUpgraded;

    private void Awake()
    {
        _isUpgraded = false;
    }

    private void Update()
    {      
        if(player.CarUpgrades[0] == true)
        {
            button.interactable = true;      
        }

        else
        {
            button.interactable = false;
        }

        if(_isUpgraded == true)
        {
            zone2button.interactable = true;
        }

        else
        {
            zone2button.interactable = false;
        } 
    }

    public void CarUpgrader()
    {
        _isUpgraded = true;
    }
}
