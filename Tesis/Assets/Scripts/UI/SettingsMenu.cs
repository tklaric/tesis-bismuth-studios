﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] Slider sfxSlider;
    [SerializeField] Slider bgmSlider;

    [SerializeField] AudioMixer sfxMixer;
    [SerializeField] AudioMixer bgmMixer;

  
    public void Start()
    {
        sfxSlider.value = 0;
        bgmSlider.value = -10;
    }

   public void SetBgmVolume(float volume)
    {
        bgmMixer.SetFloat("BackgroundMusicVolume", volume);
    }

    public void SetSfxVolume(float volume)
    {
        sfxMixer.SetFloat("SfxVolume", volume);
    }

}
