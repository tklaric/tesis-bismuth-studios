﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlowerSelector : MonoBehaviour
{
    
    [SerializeField] List<Button> flowerButtons = new List<Button>();
    [SerializeField] List<GameObject> flowerPrefabs = new List<GameObject>();
    [SerializeField] UsableWheel weaponWheel;
    [SerializeField] GameObject translucentFlower;
    [SerializeField] Player playerRef;

    private Color blockedButtonColor = new Color32(126, 126, 126, 255);
    private Color avaliableButtonColor = new Color32(255, 255, 255, 255);

    private int _flowerType;

    private bool flowerButtonsActive;

    private bool flowerSelected;

    public bool FlowerButtonsActive { get => flowerButtonsActive; set => flowerButtonsActive = value; }

    void Update()
    {
        ActivateFlowerButtons();
        CheckTranslucentFlower();
    }

    private void ActivateFlowerButtons()
    {
        if (weaponWheel.WeaponWheelActive == false)
        {
            for (int i = 0; i < flowerButtons.Count; i++)
            {
                if (flowerSelected == false)
                {
                    if (Input.GetKey(KeyCode.Q))
                    {
                        flowerButtons[i].gameObject.SetActive(true);
                        CheckPlayerFlowers();
                        Cursor.lockState = CursorLockMode.None;
                        Cursor.visible = true;
                        flowerButtonsActive = true;
                        UsableSwitcher.instance.DeactivateCurrentTool();
                    }
                }
                
                if (Input.GetKeyUp(KeyCode.Q))
                {
                    flowerButtons[i].gameObject.SetActive(false);
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    flowerButtonsActive = false;

                }
            }
        }
       
    }

    public void OnFlowerSelected() // se llama en el evento de click
    {

        flowerSelected = true;

        for (int i = 0; i < flowerButtons.Count; i++)
        {          
           flowerButtons[i].gameObject.SetActive(false);        
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        translucentFlower.SetActive(true);

    }

    public void CheckTranslucentFlower()
    {
       
        if (weaponWheel.WeaponWheelActive)
        {
            translucentFlower.SetActive(false);
            flowerSelected = false;
        }

        if (translucentFlower.activeSelf)
        {
            if (Input.GetMouseButtonDown(0))
            {
                InstantiateFlower();
                UseFlower();
            }
        }

    }

    private void CheckPlayerFlowers()
    {

        if (playerRef.ResistanceFlower <= 0)
        {
            flowerButtons[0].image.color = blockedButtonColor;
            flowerButtons[0].interactable = false;
        }
        else
        {
            flowerButtons[0].image.color = avaliableButtonColor;
            flowerButtons[0].interactable = true;
        }

        if (playerRef.PulleyResistanceFlower <= 0)
        {
            flowerButtons[1].image.color = blockedButtonColor;
            flowerButtons[1].interactable = false;
        }
        else
        {
            flowerButtons[1].image.color = avaliableButtonColor;
            flowerButtons[1].interactable = true;
        }
        if (playerRef.ShotgunFlower <= 0)
        {
            flowerButtons[2].image.color = blockedButtonColor;
            flowerButtons[2].interactable = false;
        }
        else
        {
            flowerButtons[2].image.color = avaliableButtonColor;
            flowerButtons[2].interactable = true;
        }
        if (playerRef.SuccSpeedFlower <= 0)
        {
            flowerButtons[3].image.color = blockedButtonColor;
            flowerButtons[3].interactable = false;
        }
        else
        {
            flowerButtons[3].image.color = avaliableButtonColor;
            flowerButtons[3].interactable = true;
        }
    }
       
    private void UseFlower()
    {
        if (_flowerType == 0)
        {
            playerRef.ResistanceFlower--;
        }
        if (_flowerType == 1)
        {
            playerRef.PulleyResistanceFlower--;
        }
        if (_flowerType == 2)
        {
            playerRef.ShotgunFlower--;
        }
        if (_flowerType == 3)
        {
            playerRef.SuccSpeedFlower--;
        }
    }

    public void SetSelectedFlower(int flowerType)
    {
        _flowerType = flowerType;
    }

    public void InstantiateFlower()
    {
         GameObject flowerGO =  Instantiate(flowerPrefabs[_flowerType],translucentFlower.transform);
         flowerGO.transform.parent = null;    
         translucentFlower.SetActive(false);
    }


}
