﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackBoard : MonoBehaviour
{
    Dictionary<string, object> memories = new Dictionary<string, object>();

    public object Get(string name)
    {
        if (memories.ContainsKey(name))
        {
            return memories[name];
        }

        else
        {
            return null;
        }
    }

    public void Set(string name, object value)
    {
        if (memories.ContainsKey(name))
        {
            memories[name] = value;
        }
        else
        {
            memories.Add(name, value);
        }
    }
}
