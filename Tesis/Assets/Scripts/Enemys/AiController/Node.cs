﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Edge
{
    [SerializeField] Node to;
    [SerializeField] float distance;
    [SerializeField] float weight;
    public float Weight { get => weight; set => weight = value; }
    public float Distance { get => distance; set => distance = value; }
    public Node To { get => to; set => to = value; }
}

public class Node : MonoBehaviour
{
    [SerializeField] List<Edge> edges;
    public float actualDistance = float.PositiveInfinity;
    private Node previuosNode;
    public Node PreviuosNode { get => previuosNode; set => previuosNode = value; }

    public List<Edge> Edges
    {
        get { return edges; }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        for (int i = 0; i < Edges.Count; i++)
        {
            if (Edges[i].To == null) continue;

            var diff = Edges[i].To.transform.position - transform.position;
            var arrowPos = transform.position + diff * 0.8f;

            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, Edges[i].To.transform.position);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(arrowPos, 0.25f);
        }
    }
}
