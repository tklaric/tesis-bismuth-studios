﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    bool isInTarget = false;
    bool isInSphere = false;
    public float radius = 5f;
    public float angle;
    float vectorProduct;
    float distToTarget;
    Vector3 diference;
    Transform target;
    Transform notTarget;
    public LayerMask obstacle;
    public LayerMask layer;

    public bool IsInTarget { get => isInTarget; set => isInTarget = value; }
    public bool IsInSphere { get => isInSphere; set => isInSphere = value; }
    public Transform Target { get => target; set => target = value; }
    public Transform NotTarget { get => notTarget; set => notTarget = value; }

    void Update()
    {
        target = null;
        isInTarget = false;
        isInSphere = false;


        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layer);
        for (int i = 0; i < colliders.Length; i++)
        {
            //if(colliders[i] != )
            //{
                diference = colliders[i].transform.position - transform.position;
                isInSphere = true;
                vectorProduct = Vector3.Dot(transform.forward, diference.normalized);
                if (vectorProduct > Mathf.Cos(angle * Mathf.Deg2Rad))
                {
                    distToTarget = diference.magnitude;
                    RaycastHit hit;
                    if (Physics.Raycast(transform.position, diference, out hit, distToTarget, obstacle))
                    {
                    isInTarget = false;
                        Debug.DrawLine(transform.position, hit.point, Color.red);
                    }
                    else
                    {
                        Debug.DrawLine(transform.position, colliders[i].transform.position, Color.green);
                        isInTarget = true;
                        target = colliders[i].transform;
                    }
                }
                break;
            //}           
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }


}
