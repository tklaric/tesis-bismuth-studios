﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceCalculator : MonoBehaviour
{
    [SerializeField] Transform target;
    float dist = float.MaxValue;

    public float Dist { get => dist; set => dist = value; }
    public Transform Target { get => target; set => target = value; }

    private void Update()
    {
        if (Target)
        {
            Dist = Vector3.Distance(Target.transform.position, transform.position);
        }
    }

}
