﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion_BaitState : State
{
    [SerializeField] FakeFlower flower;

    private float timer;
    private Animator anim;
    private SphereCollider tailCollider; 

    private bool baitUsed;

    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }

    public override void UpdateState(AiController ai)
    {
        if (flower.HasBeenConsumed)
        {
            baitUsed = true;
        }

        if (baitUsed)
        {
            anim.enabled = true;
            timer += Time.deltaTime;
           
            //habilitar el particle effect aca
            if (timer>=1.5)
            {
                ai.fsm.SetState("Scorpion Follow State");
            }




        }
    }

}
