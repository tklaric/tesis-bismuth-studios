﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion_AttackState : State
{
    private Animator anim;
    private SphereCollider collider;

    private float damage = 20;
    private float timer;
    private bool readyToChange;
    int colliderCoolDown = 2;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
        collider = GetComponentInParent<SphereCollider>();
    }

    public override void InitState(AiController ai)
    {
        ai.steeringManager.velocity = Vector3.zero;

        anim.SetBool("IsAttacking", true);
        anim.SetBool("IsRunning", false);

    }

    public override void UpdateState(AiController ai)
    {
        ColliderActivation();

        float radius = ai.steeringManager.radius;
        float maxSpeed = ai.steeringManager.maxSpeed;
        float maxSteering = ai.steeringManager.maxSteering;


        if (readyToChange)
        {
            ai.fsm.SetState("Scorpion Follow State");
            readyToChange = false;
        }

        if ((float)ai.memory.Get("ScorpionLife") <= 0)
        {
            ai.fsm.SetState("Scorpion Death State");
        }
    }

    public void EventReceiver()
    {
        readyToChange = true;
    }

    private void RotateToSeekingTargetLerp(AiController aI, Vector3 target)
    {
        Vector3 targetPosition = new Vector3(target.x, 0, target.z);
        Vector3 myPosition = new Vector3(aI.body.transform.position.x, 0, aI.body.transform.position.z);

        Vector3 vectorToTarget = targetPosition - myPosition;
        vectorToTarget = Vector3.Normalize(vectorToTarget);


        aI.body.transform.forward = Vector3.Lerp(aI.body.transform.forward, vectorToTarget, 1f);

    }

    void ColliderActivation()
    {
        timer += Time.deltaTime;

        if (timer >= colliderCoolDown)
        {
            collider.enabled = false;
            timer = 0;

        }
        else { collider.enabled = true; }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            Player player = other.GetComponentInChildren<Player>();

            player.PlayerTakeDamage(damage);
        }
    }


}
