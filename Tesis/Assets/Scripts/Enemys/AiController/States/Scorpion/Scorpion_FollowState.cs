﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion_FollowState : State
{

    [SerializeField] LayerMask ignoreLayer;
    [SerializeField] Transform manticoraTransform;

    private Animator anim;
    private Vector3 direction;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
        direction = new Vector3(0, -1, 0);
    }


    public override void UpdateState(AiController ai)
    {
        


        anim.SetBool("IsRunning", true);
        anim.SetBool("IsAttacking", false);

        float maxSpeed = ai.steeringManager.maxSpeed;
        float maxSteering = ai.steeringManager.maxSteering;

        ai.steeringManager.Seek((Vector3)ai.memory.Get("PlayerTransform"), maxSpeed, maxSteering);

        RotateToSeekingTargetLerp(ai, (Vector3)ai.memory.Get("PlayerTransform"));

        Invoke("RayCastToGround", 0.8f);




        if ((float)ai.memory.Get("ScorpionLife") <= 0)
        {

            ai.fsm.SetState("Scorpion Death State");
        }

        if ((float)ai.memory.Get("ScorpionDistance") <= 0.9)
        {

            ai.fsm.SetState("Scorpion Attack State"); // NO VA A CAMBIAR NUNCA DEVUETA A FOLLOW PORQUE USA UN EVENTO EN LA ANIMACION. SCORPION TIENE EL ONATTACK QUE LO VA A TENER SU ANIMACION.
        }

    }

    private void RotateToSeekingTargetLerp(AiController aI, Vector3 target)
    {
        Vector3 targetPosition = new Vector3(target.x, 0, target.z);
        Vector3 myPosition = new Vector3(aI.body.transform.position.x, 0, aI.body.transform.position.z);

        Vector3 vectorToTarget = targetPosition - myPosition;
        vectorToTarget = Vector3.Normalize(vectorToTarget);


        aI.body.transform.forward = Vector3.Lerp(aI.body.transform.forward, vectorToTarget, 1f);

    }

    private void RayCastToGround()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position + Vector3.up, direction, out hit, 100, ~ignoreLayer))
        {

            if (hit.collider.gameObject.layer == 13)
            {
                manticoraTransform.position = hit.point;
            }
        }
    }

}
