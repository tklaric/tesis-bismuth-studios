﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion_DeathState : State
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
    }

    public override void UpdateState(AiController ai)
    {
        ai.steeringManager.enabled = false;
        
        anim.SetBool("IsDead", true);
       
    }



}
