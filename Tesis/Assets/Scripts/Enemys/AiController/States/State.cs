﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    public string statename;

    public virtual void InitState(AiController ai)
    {

    }

    public virtual void UpdateState(AiController ai)
    {
        print(statename);
    }

    public virtual void EndState(AiController ai)
    {

    }
}
