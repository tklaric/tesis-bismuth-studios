﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyPatrol : State
{
    public float _distanceToChangeNode = 2;
    List<Node> node_list;
    int _currentNodeIndex = 0;
    int direction = 1;
    [SerializeField]
    Transform my_transform;

    public override void UpdateState(AiController ai)
    {
        node_list = ai.Pathfinding.FinalPath;
        var currentNode = node_list[_currentNodeIndex];

        if (_currentNodeIndex >= node_list.Count - 1)
        {
            direction = -1;
        }

        if (_currentNodeIndex <= 0)
        {
            direction = 1;
        }

        float distance = Vector3.Distance(currentNode.transform.position, transform.position);
        if (distance <= _distanceToChangeNode)
        {
            _currentNodeIndex += direction;
        }

        ai.steeringManager.Seek(currentNode.transform.position,5,5);

        //Quaternion lookOnLook = Quaternion.LookRotation(currentNode.transform.position - my_transform.position);
        //transform.rotation =Quaternion.Slerp(transform.rotation, lookOnLook, Time.deltaTime);

        my_transform.LookAt(currentNode.transform.position);
    }
}
