﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider_idleState : State
{
    Animator anim;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
    }
    public override void UpdateState(AiController ai)
    {
        ai.steeringManager.StopMovement();
        anim.SetBool("IsWalking", false);
        if ((bool)ai.memory.Get("sensorRaycast") == true)
        {
            ai.fsm.SetState("Spider Follow State");
        }

        if (((float)ai.memory.Get("distance") <= 10))
        {
            ai.fsm.SetState("Spider Follow State");
        }

    }
}
