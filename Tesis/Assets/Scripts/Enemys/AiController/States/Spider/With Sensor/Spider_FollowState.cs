﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider_FollowState : State
{
    [SerializeField] Transform seekTarget;
    [SerializeField] LayerMask layer;
    Animator anim;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
    }

    public override void UpdateState(AiController ai)
    {
        anim.SetBool("IsWalking", true);
        anim.SetBool("IsAttacking", false);
        float maxSpeed = ai.steeringManager.maxSpeed;
        float maxSteering = ai.steeringManager.maxSteering;
        
        ai.steeringManager.Separation((List<GameObject>)ai.memory.Get("neighborgs"), 35);



        ai.steeringManager.Seek((Vector3)ai.memory.Get("targetTransform"), maxSpeed, maxSteering);
        

        RotateToSeekingTargetLerp(ai, (Vector3)ai.memory.Get("targetTransform"));

        if ((float)ai.memory.Get("Life")<=0)
        {

            ai.fsm.SetState("Spider Death State");
        }

        if ((float)ai.memory.Get("distance") <= 1.8f)
        {
            ai.fsm.SetState("Spider Attack State");
        }

        if ((bool)ai.memory.Get("sensorRaycast") == false)
        {
            ai.fsm.SetState("Patrol");
        }
    }

    private void RotateToSeekingTargetLerp(AiController aI, Vector3 target)
    {
        Vector3 targetPosition = new Vector3(target.x, 0, target.z);
        Vector3 myPosition = new Vector3(aI.body.transform.position.x, 0, aI.body.transform.position.z);

        Vector3 vectorToTarget = targetPosition - myPosition;
        vectorToTarget = Vector3.Normalize(vectorToTarget);


        aI.body.transform.forward = Vector3.Lerp(aI.body.transform.forward, vectorToTarget, 1f);

    }

}
