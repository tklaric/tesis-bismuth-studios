﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider_AttackState : State
{
    [SerializeField] Transform arrivalTarget;
    Animator anim;
    SphereCollider collider;
    [SerializeField] float damage = 10;
    float timer;
    bool readyToChange;
    int colliderCoolDown = 2;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
        collider = GetComponentInParent<SphereCollider>();
    }
    private void Start()
    {
        anim.SetBool("IsAttacking", true);
        anim.SetBool("IsWalking", false);
    }

    public override void InitState(AiController ai)
    {
        ai.steeringManager.StopMovement();
        anim.SetBool("IsAttacking", true);
        anim.SetBool("IsWalking", false);
    }
    public override void UpdateState(AiController ai)
    {
        ColliderActivation();

        float radius = ai.steeringManager.radius;
        float maxSpeed = ai.steeringManager.maxSpeed;
        float maxSteering = ai.steeringManager.maxSteering;


        //ai.steeringManager.Arrival((Vector3)ai.memory.Get("targetTransform"), radius, maxSpeed, maxSteering);

        //RotateToSeekingTargetLerp(ai, (Vector3)ai.memory.Get("targetTransform"));

        if (readyToChange)
        {
            collider.enabled = false;
            ai.fsm.SetState("Spider Follow State");
            readyToChange = false;
        }

        if ((float)ai.memory.Get("distance") >= 1.8f)
        {
            collider.enabled = false;
            ai.fsm.SetState("Spider Follow State");
        }

        if ((float)ai.memory.Get("Life") <= 0)
        {
            collider.enabled = false;
            ai.fsm.SetState("Spider Death State");
        }
    }

    public void EventReceiver()
    {
        Debug.Log("evento funcando");
        readyToChange = true;
    }

    private void RotateToSeekingTargetLerp(AiController aI, Vector3 target)
    {
        Vector3 targetPosition = new Vector3(target.x, 0, target.z);
        Vector3 myPosition = new Vector3(aI.body.transform.position.x, 0, aI.body.transform.position.z);

        Vector3 vectorToTarget = targetPosition - myPosition;
        vectorToTarget = Vector3.Normalize(vectorToTarget);

       
        aI.body.transform.forward = Vector3.Lerp(aI.body.transform.forward, vectorToTarget, 1f);

    }

   

    void ColliderActivation()
    {
        timer += Time.deltaTime;

        if (timer >= colliderCoolDown)
        {
            collider.enabled = false;
            timer = 0;

        }
        else { collider.enabled = true; }
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 13)
        {
            return;
        }
        else if (other.gameObject.layer == 15)
        {
            
            Pulleys pulley = other.GetComponent<Pulleys>();
            if(pulley.DragonSuccRef.enabled)
            {
                pulley.DragonSuccRef.SuccTakeDamage(damage);
            }
            
        }
        else if (other.gameObject.layer == 8)
        {
            Player p = other.GetComponentInChildren<Player>();
            p.PlayerTakeDamage(damage);
            Register();
        }
        
        //"...Y 

    }
    void Register()
    {
        DI_System.CreateIndicator(this.transform);
    }

}
