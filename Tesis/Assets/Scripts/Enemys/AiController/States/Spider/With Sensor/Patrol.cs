﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : State
{
    Animator anim;
    public float _distanceToChangeNode = 2;
    List<Node> node_list;
    int _currentNodeIndex = 0;
    int direction = 1;
    [SerializeField]
    Transform my_transform;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
    }

    public override void UpdateState(AiController ai)
    {
        anim.SetBool("IsWalking", true);
        anim.SetBool("IsAttacking", false);

        node_list = ai.Pathfinding.FinalPath;
        var currentNode = node_list[_currentNodeIndex];


        if (_currentNodeIndex >= node_list.Count - 1)
        {
            direction = -1;
        }

        if (_currentNodeIndex <= 0)
        {
            direction = 1;
        }

        float distance = Vector3.Distance(currentNode.transform.position, transform.position);
        if (distance <= _distanceToChangeNode)
        {
            _currentNodeIndex += direction;
        }

        ai.steeringManager.Seek(currentNode.transform.position,2,2);
        my_transform.LookAt(currentNode.transform.position);

        if ((bool)ai.memory.Get("sensorRaycast") == true)
        {
            ai.fsm.SetState("Spider Follow State");
        }

        if ((float)ai.memory.Get("Life") <= 0)
        {
            ai.fsm.SetState("Spider Death State");
        }
    }
}
