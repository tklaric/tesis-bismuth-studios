﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    [SerializeField] Node origin;
    [SerializeField] Node end;
    [SerializeField] GameObject enemy;
    float actualDistance = float.PositiveInfinity;
    [SerializeField] List<Node> nodeList = new List<Node>();

    List<Node> finalPath = new List<Node>();

    public List<Node> FinalPath { get => finalPath; }

    private void Awake()
    {
        FindPath();
    }

    public void FindPath()
    {

        if(origin == end)
        {
            return;
        }

        var openSet = new List<Node>();
        openSet.Add(origin);

        for (int i = 0; i < openSet.Count; i++)
        {
            var open = openSet[i];

            for (int j = 0; j < open.Edges.Count; j++)
            {
                var edge = open.Edges[j];
                if (!openSet.Contains(edge.To))
                {
                    edge.To.PreviuosNode = open;
                    openSet.Add(edge.To);
                }

                if(edge.To == end)
                {
                    Node current = end;
                    finalPath.Add(current);

                    while(current.PreviuosNode != null)
                    {
                        current = current.PreviuosNode;
                        finalPath.Add(current);
                    }

                    finalPath.Reverse();
                    return;
                }
            }
        }
    }
}
