﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringManager : MonoBehaviour
{
    AiController aiController;

    public Vector3 velocity;
    public LayerMask layer;
    public float maxSpeed;
    public float maxSteering;
    public float secondsToPredict;
    public float fleeVelocity;
    public float radius;
    public float separationDistance = 2;
    private float coefLimit = 0.15f;

    void Awake()
    {
        aiController = GetComponent<AiController>();
    }

    void Update()
    {
        aiController.body.transform.position += velocity * Time.deltaTime;
    }

    public void StopMovement()
    {
        Vector3 nullVelocity = new Vector3(0, 0, 0);
        velocity = nullVelocity;
    }

    public void Seek(Vector3 targetPosition, float maxSpeed, float steeringSpeed)
    {
        Vector3 diff = targetPosition - aiController.body.transform.position;
        Vector3 dir = Vector3.Normalize(diff);
        Vector3 desiredVelocity = dir * maxSpeed;

        Vector3 velDiff = desiredVelocity - velocity;
        Vector3 velDir = Vector3.Normalize(velDiff);
        Vector3 steeringForce = velDir * steeringSpeed;


        velocity += steeringForce * Time.deltaTime;
    }

    public void Flee(Vector3 targetPosition)
    {
        Vector3 diff = aiController.body.transform.position - targetPosition;
        Vector3 dir = Vector3.Normalize(diff);
        Vector3 desiredVelocity = dir * maxSpeed;

        Vector3 velDiff = desiredVelocity - velocity;
        Vector3 velDir = Vector3.Normalize(velDiff);
        Vector3 steeringForce = velDir * fleeVelocity;

        velocity += steeringForce * Time.deltaTime;
    }

    public void Arrival(Vector3 targetPosition, float radius, float maxSpeed, float steeringSpeed)
    {
        Vector3 bodyPos = aiController.body.transform.position;
        float dist = Vector3.Distance(bodyPos, targetPosition);
        float distCoef = Mathf.Clamp(dist / radius, 0, 1);

        Vector3 diff = targetPosition - aiController.body.transform.position;
        Vector3 dir = Vector3.Normalize(diff);
        Vector3 desiredVelocity = dir * maxSpeed * distCoef;

        Vector3 velDiff = desiredVelocity - velocity;
        Vector3 velDir = Vector3.Normalize(velDiff);
        Vector3 steeringForce = velDir * steeringSpeed;

        velocity += steeringForce * Time.deltaTime;
        if (distCoef < coefLimit)
        {
            velocity = new Vector3(0, 0, 0);
        }
    }

    public void Pursuit(Vector3 targetPos, Vector3 targetVel, float secondsToPredict, float maxSpeed, float steeringSpeed)
    {
        Vector3 futurePos = targetPos + targetVel * secondsToPredict;
        Seek(futurePos, maxSpeed, steeringSpeed);
    }

    public void Evade(Vector3 targetPos, Vector3 targetVel)
    {
        Vector3 futurePos = secondsToPredict * targetVel + targetPos;
        Flee(futurePos);
    }


    public void ObstacleAvoidance(float lengthMultiplier, float pushMagnitude, float steeringSpeed)
    {
        RaycastHit hit;
        Vector3 diff = velocity * lengthMultiplier; // calculo para ver a mas adelante
        float dist = diff.magnitude; //se saca el valor de ese diff

        if (Physics.Raycast(transform.position, velocity, out hit, dist, layer))
        {
            Vector3 transformDiference = transform.position - hit.transform.position; // para evitar bug con rectangulo

            Vector3 desiredVelocity = Vector3.Normalize(transformDiference * steeringSpeed);  //normaliza la desired con la velocidad de steering para despues sacar la dir.


            Vector3 velDir = Vector3.Normalize(desiredVelocity - velocity);//direccion de desiredvelocity
            velocity += velDir * pushMagnitude * Time.deltaTime;
        }


    }

    public void Separation(List<GameObject> neighborgs, float forceMultiplier)
    {
        Vector3 vector = new Vector3(0, 0, 0);
        int neighbourCount = 0;

        for (int i = 0; i < neighborgs.Count; i++)
        {
            var currentNeighbour = neighborgs[i];
            if (currentNeighbour != null)
            {
                Vector3 myposition = aiController.body.transform.position;
                Vector3 neighboursPosition = currentNeighbour.transform.position;
                float distance = Vector3.Distance(myposition, neighboursPosition);

                if (distance < separationDistance)
                {
                    vector.x += neighboursPosition.x - myposition.x; // calcula la direccion del vector desde myposition a neighbour
                    vector.z += neighboursPosition.z - myposition.z;
                    neighbourCount++;
                }
            }
        }

        if (neighbourCount == 0) return;

        vector.x /= neighbourCount; // dependiendo la cantidad de enemigos va a tener mas o menos potencia
        vector.z /= neighbourCount;


        vector.x *= -1; //para que la direccion sea al reves
        vector.z *= -1;

        vector.Normalize();

        velocity += vector * Time.deltaTime * forceMultiplier;
    }



}
