﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionController : AiController
{
    [SerializeField] DistanceCalculator distanceCalculator;

    private Scorpion scorpion;

    private List<GameObject> neighborgs = new List<GameObject>();

    public List<GameObject> Neighborgs { get => neighborgs; set => neighborgs = value; }

    public override void Start()
    {
        base.Awake();

        scorpion = GetComponentInParent<Scorpion>();
        distanceCalculator.Target = FindObjectOfType<Player>().transform;

        memory.Set("Scorpion", body);
        memory.Set("ScorpionDistance", distanceCalculator.Dist);
        memory.Set("ScorpionNeighborgs", neighborgs);

    }

    public override void Update()
    {
        memory.Set("ScorpionLife", scorpion.Life);
        memory.Set("ScorpionDistance", distanceCalculator.Dist);
        memory.Set("PlayerTransform", distanceCalculator.Target.position);

    }



}
