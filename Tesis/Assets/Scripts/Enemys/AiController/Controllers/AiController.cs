﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : MonoBehaviour
{
    public GameObject body;

    public BlackBoard memory;

    [SerializeField] Pathfinding pathfinding;

    public FSM fsm;  

    public SteeringManager steeringManager;

    public Pathfinding Pathfinding { get => pathfinding; set => pathfinding = value; }

    

    public void Awake()
    {
        memory = GetComponent<BlackBoard>();
        steeringManager = GetComponent<SteeringManager>();
        fsm = GetComponent<FSM>();
    }

    public virtual void Start() { }

    public virtual void Update()
    {
      
    }
}
