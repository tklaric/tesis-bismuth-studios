﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : AiController
{
    [SerializeField] DistanceCalculator distanceCalculator;
    [SerializeField] Transform playerTransform;
    [SerializeField] Transform pulleyTransform;
    [SerializeField] Sensor sensor;
    private List<GameObject> neighborgs = new List<GameObject>();

    private Spider spider;

    public List<GameObject> Neighborgs { get => neighborgs; set => neighborgs = value; }

    private void Awake()
    {
        sensor = GetComponent<Sensor>();
    }

    public override void Start()
    {
        base.Awake();

        playerTransform = FindObjectOfType<Player>().transform;
        spider = GetComponentInParent<Spider>();
        //distanceCalculator.Target = playerTransform; //default distanceCalculator target es player
        memory.Set("body", body);
        memory.Set("sensor", sensor);
        memory.Set("distance", distanceCalculator.Dist);
        memory.Set("neighborgs", Neighborgs);
        memory.Set("sensorRaycast", sensor.IsInTarget);
        memory.Set("targetplayer", sensor.Target);
        memory.Set("targetobstacle", sensor.NotTarget);
        memory.Set("Life", spider.life);
    }

    // Update is called once per frame
    public override void Update()
    {
        memory.Set("Life", spider.life);
        memory.Set("distance", distanceCalculator.Dist);
        memory.Set("targetTransform", distanceCalculator.Target.position);
        memory.Set("targetobstacle", sensor.NotTarget);
        memory.Set("targetplayer", sensor.Target);
        memory.Set("sensorRaycast", sensor.IsInTarget);
        memory.Set("sensorOverlap", sensor.IsInSphere);

    }

    public void PickTarget(int targetNumber)
    {

        if (targetNumber == 0)
        {
            distanceCalculator.Target = playerTransform;
        }
        else
        {
            distanceCalculator.Target = pulleyTransform;
        }
    }
    public void GetPlayerTransform(Transform playerTransform)
    {
        this.playerTransform = playerTransform;

    }
    public void GetTargetsTransform(Transform playerTransform, Transform pulleyTransform)
    {
        this.playerTransform = playerTransform;
        this.pulleyTransform = pulleyTransform;
    }
}
