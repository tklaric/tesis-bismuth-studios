﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
    public List<State> states;
    public State currentstate;
    AiController aicontroller;

    private void Awake()
    {
        aicontroller = GetComponent<AiController>();
    }

    public void Start()
    {
        currentstate.InitState(aicontroller);
    }

    public void Update()
    {
        currentstate.UpdateState(aicontroller);
    }

    public void SetState(string name)
    {
        for (int i = 0; i < states.Count; i++)
        {
            if(states[i].statename == name)
            {
                if(currentstate != null)
                {
                    currentstate.EndState(aicontroller);
                }

                currentstate = states[i];
                currentstate.InitState(aicontroller);

                break;
            }
        }
    }
}
