﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using THOR;

public class Dragon : MonoBehaviour , IHigherEnemy
{
    [SerializeField] List<Transform> myHitPoints;
    [SerializeField] List<Transform> myMissPoints;
    [SerializeField] Transform cameraLookAtThis;
    [SerializeField] Animator anim;
    [SerializeField] Transform playerTransform;
    [SerializeField] Transform rockSpawnerTransform;
    [SerializeField] Player player;
    [SerializeField] GameObject aicontroller;
    [SerializeField] BloodBucket bloodBucketReference;
    [SerializeField] CrossbowShoot crossbow;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource wingFlaps;

    [SerializeField] GameObject rockPrefab;
    [SerializeField] GameObject portalEffect;
    [SerializeField] GameObject lightSignalPrefab;
    [SerializeField] GameObject thunderStormPrefab; // mover estas 3 lineas al gamemanager
    [SerializeField] ParticleSystem dustStorm;

    [SerializeField] Color semiBerserkColor;
    [SerializeField] Color berserkColor;
    [SerializeField] Color startColor;

    [SerializeField] Light directionalLight;
    [SerializeField] AudioClip succHitSound;
    [SerializeField] AudioClip hitSound;
    [SerializeField] AudioClip deathSound;

    [SerializeField] EnemySpawner enemySpawnerRef;
    [SerializeField] LayerMask ignoreMe;

    private Vector3 direction;
    private DragonSucc dragonSuccRef;
    private BoxCollider _myBoxCollider;
    private Arrow arrowThatHittedMe;

    private int pulleyShotsReceived;
    private bool gettingSuccd = false;

    float actualTimeDrain;
    float timeToDrain = 10;
    float distanceFromPlayer;
    float minDistanceFromPlayer = 3f;

    bool dragonDied = false;

    public int PulleyShotsReceived { get => pulleyShotsReceived; set => pulleyShotsReceived = value; }
    public bool DragonDied { get => dragonDied; set => dragonDied = value; }
    public bool GettingSuccd { get => gettingSuccd; set => gettingSuccd = value; }
    public Transform CameraLookAtThis { get => cameraLookAtThis; }
    public List<Transform> MyMissPoints { get => myMissPoints; set => myMissPoints = value; }

    private void Start()
    {
        _myBoxCollider = GetComponent<BoxCollider>();
        dragonSuccRef = GetComponent<DragonSucc>();
        RenderSettings.skybox.SetColor("_Tint", startColor);
        direction = new Vector3(0, -1, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        arrowThatHittedMe = collision.gameObject.GetComponent<Arrow>();
        arrowThatHittedMe.Pulley.HigherBeing = this;
        collision.collider.enabled = false;
        

        if (crossbow.IsLethal)
        {
            if (arrowThatHittedMe.Pulley.HasChainConnected == false)
            {
                PulleyShotsReceived++;
                audioSource.clip = hitSound;
                audioSource.Play();
                arrowThatHittedMe.Pulley.HasChainConnected = true;
                arrowThatHittedMe.Pulley.actualConnection.Line.enabled = true;

                if (PulleyShotsReceived == 1)
                {
                    anim.SetTrigger("HasBeenHitted");
                    GameObject thunderStormGO = Instantiate(thunderStormPrefab, playerTransform.transform); // todo esto tiene que ir al game manager
                    THOR_Thunderstorm.SetProbability(0.2f);
                    THOR_Thunderstorm.instance.spawnHeight = 300;
                    THOR_Thunderstorm.instance.scaleMulti = 30;

                    RenderSettings.skybox.SetColor("_Tint", semiBerserkColor);

                    directionalLight.color = semiBerserkColor; //hasta aca

                    StartCoroutine(SpawnSpiders(6));

                }
                else if (PulleyShotsReceived == 2)
                {
                    THOR_Thunderstorm.SetProbability(0.75f);

                    RenderSettings.skybox.SetColor("_Tint", berserkColor);


                    directionalLight.color = berserkColor;

                    Color32 rgb = new Color32(104, 91, 63, 255);
                    ParticleSystem.MainModule settings = dustStorm.main;
                    settings.startColor = new ParticleSystem.MinMaxGradient(rgb);

                    StartCoroutine(SpawnSpiders(6));

                    //eventualmente este pedazo moverlo al 4to
                    audioSource.clip = deathSound;
                    audioSource.Play();
                    anim.SetBool("IsDead", true);
                    dragonDied = true;
                    _myBoxCollider.enabled = true;
                    aicontroller.SetActive(false);
                }
              
            }
            
        }
        else
        {
            if (arrowThatHittedMe.Pulley.HasSuccConnected == false) 
            {
                audioSource.clip = succHitSound;
                audioSource.Play();
                Invoke("SpawnSignalEffect", 2f);
                arrowThatHittedMe.Pulley.HasSuccConnected = true;
                arrowThatHittedMe.Pulley.actualConnection.Line.enabled = true;
                dragonSuccRef.BloodBucketReference = arrowThatHittedMe.Pulley.BloodBucketConnected;
                dragonSuccRef.CrossbowReference = arrowThatHittedMe.Pulley.Crossbow;
                dragonSuccRef.enabled = true;
                dragonSuccRef.OnSuccEventCompleted += ShootFireBall;
                dragonSuccRef.HarpoonSuccStats = arrowThatHittedMe.Pulley.Crossbow.succStatsRef;
            }

        }
    }
   
    private void Update()
    {
        CalculateDistance();
        CheckDragonDeath();
       
    }

    IEnumerator SpawnSpiders(int ammount)
    {
        yield return new WaitForSeconds(3);
        enemySpawnerRef.SpawnSpiders(arrowThatHittedMe.Pulley.SpiderSpawnPoints, ammount, true);
        
    }

    private void SpawnSignalEffect()
    {
        GameObject lightSignalGO = Instantiate(lightSignalPrefab, transform);
    }

    private void ShootFireBall()
    {
        // reproducir sonido de dragon que indique quilombo
        Invoke("InstantiatePortalEffect", 2f);
        Debug.LogError("ACORDATE DE PONERME UN SONIDO PARA EL DRAGON(PERSONA ASIGNADA: EMA)");
    }

    private void InstantiatePortalEffect()
    {
        GameObject portalGO = Instantiate(portalEffect, this.transform);
        portalGO.transform.localRotation =new Quaternion(0, 0, 0,0);
        portalGO.transform.localScale+=new Vector3(2, 2, 2);
        portalGO.transform.localPosition += new Vector3(0, 3, 25);

        GameObject rockGO = Instantiate(rockPrefab, rockSpawnerTransform);
        RockMovement rockMovement = rockGO.AddComponent<RockMovement>();
        rockMovement.PortalPosition = portalGO.transform.position;
    }

    void CheckDragonDeath()
    {

        if (DragonDied)
        {

            wingFlaps.Stop();

            RaycastHit hit;

            if (Physics.Raycast(transform.position, direction, out hit, 1000, ~ignoreMe))
            {
                if (hit.collider.gameObject.layer == 13)
                {
                    transform.position += direction;
                    
                    anim.SetBool("IsGrounded", true);
                    SaveDragonDeath();                   
                }
            }
        }
    }

    void CalculateDistance()
    {
        distanceFromPlayer = Vector3.Distance(playerTransform.transform.position, transform.position);
    }
    public Transform GiveTarget()
    {
        return myHitPoints[pulleyShotsReceived];
    }

    public void SaveDragonDeath()
    {
        SaveData.current.SaveDragonDeath(dragonDied, transform.position);
        SerializationManager.Save("SaveData", SaveData.current);
    }

}
