﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHigherEnemy
{
    int PulleyShotsReceived
    {
        get;
        set;
    }
}
