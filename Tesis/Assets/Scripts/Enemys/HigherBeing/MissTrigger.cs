﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissTrigger : MonoBehaviour
{

    private Arrow arrow;

    private void OnTriggerEnter(Collider other)
    {
        arrow = other.GetComponent<Arrow>();

        if (arrow != null)
        {
            arrow.GetComponent<SphereCollider>().enabled = false;
            arrow.enabled = false;
            //arrow.Velocity = new Vector3(arrow.Velocity.x, arrow.Velocity.y / 2, arrow.Velocity.z);
            arrow.rb.velocity = arrow.Velocity;
            arrow.TargetHitted();
            Destroy(arrow.gameObject, 5f);
        }   
    }
   
}
