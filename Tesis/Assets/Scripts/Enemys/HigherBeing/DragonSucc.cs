﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DragonSucc : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] Scrollbar higherBloodBar;
    [SerializeField] Scrollbar pulleyHp;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip completedSound;
    [SerializeField] TextMeshProUGUI completedFailedText;
    [SerializeField] TextMeshProUGUI buffTimerText;

    public SpiderSpawner spiderSpawnerRef;

    private BloodBucket bloodBucketReference;
    private CrossbowShoot crossbowReference;
    private succStats harpoonSuccStats;
    private bool hasResistanceBuff;
    private bool hasSpeedBuff;

    private Color32 succesColor = new Color32(106, 202, 156,255);
    private Color32 failColor = new Color32(197, 116, 96,255);
    private float randomLineBreakTime; //aleatorizar un valor que sea +-5 a el lineStrenght. Que te avise 8 segundos antes de que se corte la soga y que solo ahi se pueda
                                       //interactuar para volver a darle un valor nuevo 

    private int succsCompleted;
    private bool succBroken;

    public delegate void OnSuccCompleted();
    public OnSuccCompleted OnSuccEventCompleted;

    public BloodBucket BloodBucketReference { get => bloodBucketReference; set => bloodBucketReference = value; }
    public CrossbowShoot CrossbowReference { get => crossbowReference; set => crossbowReference = value; }
    public succStats HarpoonSuccStats { get => harpoonSuccStats; set => harpoonSuccStats = value; }
    public bool SuccBroken { get => succBroken; set => succBroken = value; }
    public bool HasResistanceBuff { get => hasResistanceBuff; set => hasResistanceBuff = value; }
    public bool HasSpeedBuff { get => hasSpeedBuff; set => hasSpeedBuff = value; }
    public TextMeshProUGUI BuffTimerText { get => buffTimerText; set => buffTimerText = value; }
    public int SuccsCompleted { get => succsCompleted; set => succsCompleted = value; }
    public Scrollbar HigherBloodBar { get => higherBloodBar; set => higherBloodBar = value; }

    //el succ speed va a determinar q tan rapido se llena el bloodbucket
    //lineStrenght va a determinar cada cuanto se corta la soga si no se atiende 
    //lineHp va a determinar cuando se corta la soga respecto a golpes de enemigos
    //bloodbucket tiene q tener su propia cantidad en litros a llenar.
    private void Awake()
    {
        Debug.Log(succsCompleted);
    }
    private void Update()
    {
        if(HasSpeedBuff)
        {
            bloodBucketReference.actualBloodLiters += Time.deltaTime * (HarpoonSuccStats.lineSpeed*1.50f);
        }
        else
        {
            bloodBucketReference.actualBloodLiters += Time.deltaTime * HarpoonSuccStats.lineSpeed;
        }


        pulleyHp.size = HarpoonSuccStats.lineHp / HarpoonSuccStats.MaxLineHp;
        HigherBloodBar.size = bloodBucketReference.actualBloodLiters / bloodBucketReference.maxBloodLiters; //lleno la barra de sangre del dragon
        bloodBucketReference.BloodFill.transform.localScale = new Vector3(bloodBucketReference.BloodFill.transform.localScale.x,bloodBucketReference.actualBloodLiters / bloodBucketReference.maxBloodLiters, bloodBucketReference.BloodFill.transform.localScale.z);

        if (bloodBucketReference.actualBloodLiters>25+(25*SuccsCompleted))
        {
            SuccsCompleted++;
            crossbowReference.PulleyReference.SphereCollider.radius *= 2; // agranda collider de ppulley post arana
            HigherBloodBar.gameObject.SetActive(false); //desactivo la barra cuando se llena
            pulleyHp.gameObject.SetActive(false);

            audioSource.clip = completedSound;
            audioSource.Play();
            completedFailedText.enabled = true;
            completedFailedText.text = "succ completed";
            completedFailedText.color = succesColor;
            succesColor.a -= 15;


            if (SuccsCompleted == 4)
            {
                bloodBucketReference.hasAncientBlood = true;// TODOS LOS EVENTOS COMPLETADOS
            }
            bloodBucketReference.actualBloodLiters = 25 * SuccsCompleted;
            bloodBucketReference.DisconnectFromPulley(); //  PONER ESTO CON BOLA DE FUEGO DEL DRAGON PARA QUE DESCONECTE

            //todo lo q tenga q pasar cuando se termina el SUCC
            CrossbowReference.PulleyReference.pulleyManager.RequestCrossbowDespawn();
            crossbowReference.PulleyReference.HasSuccConnected = false;          
            CrossbowReference.PulleyReference.actualConnection.Line.enabled = false;
            OnSuccEventCompleted();
            CrossbowReference.PulleyReference.SuccEventCompleted();
            crossbowReference.PulleyReference.BlockPulley();
            SaveSuccEvent(); //SAVE CUADNO SE COMPLETA EL EVENTO
            this.enabled = false;
        }
    }
    private void OnEnable()
    {
        HigherBloodBar.gameObject.SetActive(true);
        pulleyHp.gameObject.SetActive(true);
        gameManager.Pulley = crossbowReference.PulleyReference;
        crossbowReference.PulleyReference.SphereCollider.radius /= 2;

        if(bloodBucketReference.actualBloodLiters>bloodBucketReference.maxBloodLiters)
        {
            crossbowReference.PulleyReference.SphereCollider.radius *= 2;
            HigherBloodBar.gameObject.SetActive(false); // desactivo la barra si la misma ya fue llenada
            pulleyHp.gameObject.SetActive(false);
            BloodBucketReference.DisconnectFromPulley();
        }
    }

    public void SaveSuccEvent()
    {
        PlayerMovement player = FindObjectOfType<PlayerMovement>();
        SaveData.current.SaveSuccEvent(SuccsCompleted, bloodBucketReference.actualBloodLiters, crossbowReference.PulleyReference.IsPulleyBlocked,HigherBloodBar.size, bloodBucketReference.BloodFill.transform.localScale, player.transform.position);
        SerializationManager.Save("SaveData", SaveData.current);
    }

    private void disableDelay()
    {
        this.enabled = false;
    }

    private void OnDisable()
    {
        Invoke("KillAllSpiders", 8);
    }

    private void KillAllSpiders()
    {
        spiderSpawnerRef.KillAllSpiders();
    }

    public void SuccTakeDamage(float damage)
    {
        if(HasResistanceBuff)
        {
            harpoonSuccStats.lineHp -= damage / 2;
        }
        else
        {
            harpoonSuccStats.lineHp -= damage;
        }
        

        if (harpoonSuccStats.lineHp <= 0) 
        {
            SuccBroken = true;
            audioSource.Play();
            bloodBucketReference.DisconnectFromPulley();
            HigherBloodBar.gameObject.SetActive(false); // desactivo la barra de sangre si el cable fue destruido
            pulleyHp.gameObject.SetActive(false);
            bloodBucketReference.actualBloodLiters = 0;
            crossbowReference.PulleyReference.pulleyManager.RequestCrossbowDespawn();
            crossbowReference.PulleyReference.SphereCollider.radius *= 2;
            crossbowReference.PulleyReference.InstantiateEventFailedEffect();
            crossbowReference.PulleyReference.HasSuccConnected = false;
            CrossbowReference.PulleyReference.actualConnection.Line.enabled = false;
            bloodBucketReference.BloodFill.transform.localScale = new Vector3(bloodBucketReference.BloodFill.transform.localScale.x, bloodBucketReference.actualBloodLiters / bloodBucketReference.maxBloodLiters, bloodBucketReference.BloodFill.transform.localScale.z);
            this.enabled = false;
        }
    }

}
