﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour, IGroundEnemy
{
    public float life = 100;
    
    [SerializeField] Spider_AttackState attackStateRef;
    [SerializeField] LayerMask ignoreLayer;
    [SerializeField] List<AudioClip> spiderSounds = new List<AudioClip>();
    [SerializeField] GameObject eventCompletedEffectPrefab;

    private EnemySpawner enemySpawnerRef;
    private AudioSource audioSrc;
    private Vector3 direction;
    private bool startMadAtPlayer;
    private bool isSubscribedToDeathEvent;
    private bool isChainEventSpider;

    public AiController spiderControllerRef;

    Animator anim;

    public bool StartMadAtPlayer { get => startMadAtPlayer; set => startMadAtPlayer = value; }
    public bool IsSubscribedToDeathEvent { get => isSubscribedToDeathEvent; set => isSubscribedToDeathEvent = value; }
    public bool IsChainEventSpider { get => isChainEventSpider; set => isChainEventSpider = value; }

    public delegate void Die(Spider spider);
    public event Die OnDeath;

    private void Awake()
    {
        anim = GetComponent<Animator>();

        audioSrc = GetComponent<AudioSource>();

        direction = new Vector3(0, -1, 0);
        enemySpawnerRef = GameObject.FindObjectOfType<EnemySpawner>();
    }
    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position+Vector3.up, direction, out hit, 100, ~ignoreLayer))
        {
            if (hit.collider.gameObject.layer == 13)
            {
                transform.position = hit.point;
                //transform.rotation = Quaternion.LookRotation(hit.normal);
                //transform.Rotate(77, 0, 0);
            }
        }
    }

    public void TakeDamage(float damage)
    {
        life -= damage;

        if (life <= 0)
        {
            if(isSubscribedToDeathEvent == true)
            {           
                OnDeath(this);
            }
            if (enemySpawnerRef.SpidersAlive.Count == 0 && IsChainEventSpider)
            {
                GameObject effectGO = Instantiate(eventCompletedEffectPrefab, this.transform);
                effectGO.transform.position += new Vector3(0, 5, 0);
                
            }
            enemySpawnerRef.SpidersAlive.Remove(this);

        }
    }

    private void DeathEvent()
    {
        Destroy(gameObject);
    }

    private void OnAttackEnd()
    {
        
        int random = Random.Range(0, spiderSounds.Count);

        audioSrc.clip = spiderSounds[random];
        audioSrc.Play();
       

        attackStateRef.EventReceiver();
    }

    
}
