﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerSpiderSpawner : MonoBehaviour
{
    [SerializeField] List<Transform> spawnPoints = new List<Transform>();
    [SerializeField] GameObject spiderPrefab;
    [SerializeField] Transform playerTransformRef;
    [SerializeField] float beginSpawningAfter;
    [SerializeField] float timeBetweenGroupSpawn;

    private float initialTimeBetweenSpawns;
    private bool spawnAllowed;
    private float timer;
    private List<GameObject> neighborgs = new List<GameObject>();
    private List<Spider> spiders = new List<Spider>();


    public List<Transform> SpawnPoints { get => spawnPoints; set => spawnPoints = value; }

    private void Awake()
    {
        initialTimeBetweenSpawns = timeBetweenGroupSpawn;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if(timer>beginSpawningAfter && !spawnAllowed)
        {
            spawnAllowed = true;
            timer = 0;
            int random = Random.Range(1, 4);
            SpawnGroup(random);
        }
        if (timer > timeBetweenGroupSpawn && spawnAllowed)
        {
            timer = 0;
            int random = Random.Range(1, 4);
            SpawnGroup(random);
            float randomSpawnTimerModifier = Random.Range(initialTimeBetweenSpawns - 1, initialTimeBetweenSpawns + 1);
            timeBetweenGroupSpawn = randomSpawnTimerModifier;
        }
    }

    public void SpawnSpider(int index)
    {
        GameObject spiderGO = Instantiate(spiderPrefab, spawnPoints[index]);
        spiderGO.transform.parent = null;
        spiderGO.transform.localScale += spiderGO.transform.localScale * 1.5f;
        SpiderControllerWithoutSensor spiderControllerRef = spiderGO.GetComponentInChildren<SpiderControllerWithoutSensor>();
        spiderControllerRef.GetTargetsTransform(playerTransformRef, null); //el segundo no es necesario ya q no se selecciona una pulley
        neighborgs.Add(spiderGO);
        spiderControllerRef.Neighborgs = neighborgs;
        spiderControllerRef.PickTarget(0);
        var newSpider = spiderGO.GetComponentInChildren<Spider>();
        spiders.Add(newSpider);
        newSpider.OnDeath += RemoveSpider;
        newSpider.IsSubscribedToDeathEvent = true;
    }

    private void RemoveSpider(Spider spider)
    {
        spiders.Remove(spider);
        spider.OnDeath -= RemoveSpider;
    }

    public void KillAllSpiders()
    {
        List<Spider> auxSpiderList = new List<Spider>();
        for (int i = 0; i < spiders.Count; i++)
        {
            auxSpiderList.Add(spiders[i]);
        }
        for (int i = 0; i < auxSpiderList.Count; i++)
        {
            auxSpiderList[i].TakeDamage(9999);
        }

    }
    public void SpawnGroup(int ammountOfSpiders)
    {
        List<int> auxList = new List<int>();

        for (int i = 0; i < spawnPoints.Count; i++)
        {
            auxList.Add(i);
        }
        for (int i = 0; i < ammountOfSpiders; i++)
        {
            int aux = Random.Range(0, auxList.Count);

            SpawnSpider(auxList[aux]);
            auxList.RemoveAt(aux);
        }
    }
}
