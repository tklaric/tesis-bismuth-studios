﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderSpawner : MonoBehaviour
{
    [SerializeField] List<Transform> spawnPoints = new List<Transform>();
    [SerializeField] GameObject spiderPrefab;

    private List<GameObject> neighborgs = new List<GameObject>();
    private List<Spider> spiders = new List<Spider>();
   
    private GameManager gameManager;

    public List<Transform> SpawnPoints { get => spawnPoints; set => spawnPoints = value; }

    private void Awake()
    {
        gameManager  = GetComponent<GameManager>();
    }
    
    public void SpawnSpider(int index)
    {        
        GameObject spiderGO = Instantiate(spiderPrefab, spawnPoints[index]);
        SpiderControllerWithoutSensor spiderControllerRef = spiderGO.GetComponentInChildren<SpiderControllerWithoutSensor>();
        spiderControllerRef.GetTargetsTransform(gameManager.Player.transform, gameManager.Pulley.transform);
        neighborgs.Add(spiderGO);
        spiderControllerRef.Neighborgs = neighborgs;
        int random = Random.Range(0, 2);
        spiderControllerRef.PickTarget(random);
        var newSpider = spiderGO.GetComponentInChildren<Spider>();
        spiders.Add(newSpider);
        newSpider.OnDeath += RemoveSpider;
        newSpider.IsSubscribedToDeathEvent = true;
    }

    private void RemoveSpider(Spider spider)
    {
        spiders.Remove(spider);
        spider.OnDeath -= RemoveSpider;
    }

    public void KillAllSpiders()
    {
        List<Spider> auxSpiderList = new List<Spider>();
        for (int i = 0; i < spiders.Count; i++)
        {
            auxSpiderList.Add(spiders[i]);
        }
        for (int i = 0; i < auxSpiderList.Count; i++)
        {
            
            Debug.LogWarning(i + " yo " + spiders.Count, auxSpiderList[i].gameObject);
            auxSpiderList[i].TakeDamage(9999);
        }

    }
    public void SpawnGroup(int ammountOfSpiders)
    {
        List<int> auxList = new List<int>();

        for (int i = 0; i < spawnPoints.Count; i++)
        {
            auxList.Add(i);
        }
        for (int i = 0; i < ammountOfSpiders; i++)
        {
            int aux = Random.Range(0, auxList.Count);

            SpawnSpider(auxList[aux]);
            auxList.RemoveAt(aux);
        }
    }

}
