﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion : MonoBehaviour, IGroundEnemy
{

    [SerializeField] Scorpion_AttackState attackStateRef;
    [SerializeField] AudioSource audiosrc;
    [SerializeField] List<AudioClip> manticoraSounds = new List<AudioClip>();
    [SerializeField] float life = 200;
    //[SerializeField] LayerMask ignoreLayer;


    private Vector3 direction;
    private Animator scorpionAnimator;
    private bool startMadAtPlayer;

    public bool StartMadAtPlayer { get => startMadAtPlayer; set => startMadAtPlayer = value; }
    public float Life { get => life; set => life = value; }

    public delegate void Die(Scorpion scorpion);
    public event Die OnDeath;


    private void Awake()
    {
        scorpionAnimator = GetComponent<Animator>();

        direction = new Vector3(0, -1, 0);

        StartMadAtPlayer = false;

    }

    private void Update() // por las dudas dejo el raycast porque no sabemoscomo va a interactuar el modelo dle segundo enemigo
    {
       
    }

    public void TakeDamage(float damage)
    {
        Life -= damage;
        scorpionAnimator.SetTrigger("HasBeenHitted");
        if (Life<=0)
        {
            Destroy(this.gameObject, 3f);
            //OnDeath(this);
            
        }
        
    }

    private void DeathEvent()
    {
        Destroy(gameObject);
    }

    private void OnAttackEnd() // SETEARLO AL FINAL DE LA ANIMACION DEL MODELO
    {
        int random = Random.Range(0, manticoraSounds.Count);

        audiosrc.clip = manticoraSounds[random];
        audiosrc.Play();

        attackStateRef.EventReceiver();
    }

}
