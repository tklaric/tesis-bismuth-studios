﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGroundEnemy
{
    void TakeDamage(float damage);

    bool StartMadAtPlayer { get; set; }
}
