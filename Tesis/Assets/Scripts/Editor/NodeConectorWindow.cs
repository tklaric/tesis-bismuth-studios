﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NodeConectorWindow : EditorWindow
{
    float minimumDistance;
    int layer;


    [MenuItem("AI/Node Conector")]
    public static void ShowWindow()
    {
        NodeConectorWindow window = GetWindow<NodeConectorWindow>();
        window.Show();
    }

    void OnGUI()
    {
        minimumDistance = EditorGUILayout.Slider("Minimum distance", minimumDistance, 0, 30);
        layer = EditorGUILayout.LayerField("Layer", layer);

        if (GUILayout.Button("Search nodes"))
        {
            Node[] allNodes = Object.FindObjectsOfType<Node>();

            for (int i = 0; i < allNodes.Length; i++)
            {
                Node selectedNode1 = allNodes[i]; // agrego el nodo seleccionado al array
                selectedNode1.Edges.Clear();

                for (int e = 0; e < allNodes.Length; e++)
                {
                    Node selectedNode2 = allNodes[e];
                    if (selectedNode1 == selectedNode2) continue; // pregunto si pertenece a un nodo seleccionado

                    float dist = Vector3.Distance(selectedNode1.transform.position, selectedNode2.transform.position);//calculo la distancia entre los nodos
                    Vector3 diff = selectedNode2.transform.position - selectedNode1.transform.position;


                    if (dist < minimumDistance && !Physics.Raycast(selectedNode1.transform.position, diff, dist, 1 << layer)) // limito la distancia de busqueda dependiendo del valor del slider
                    {
                        Edge newEdge = new Edge();
                        newEdge.To = selectedNode2;
                        newEdge.Distance = dist;

                        selectedNode1.Edges.Add(newEdge);
                    }
                }
            }
        }
    }

}
