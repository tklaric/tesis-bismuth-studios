﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuccSpeedFlower : MonoBehaviour
{
    private int counter;
    private float timer = 30;
    private bool flowerInstantiated;
    private DragonSucc dragonSuccRef;
    [SerializeField] GameObject succSpeedEffect;

    private void Awake()
    {
        flowerInstantiated = true;
    }

    private void Update()
    {
        if (flowerInstantiated && dragonSuccRef != null)
        {
            timer -= Time.deltaTime;
            dragonSuccRef.BuffTimerText.text = timer.ToString("f0");
            if (timer <= 0)
            {
                timer = 0;
                dragonSuccRef.BuffTimerText.gameObject.SetActive(false);
                dragonSuccRef.HasSpeedBuff = false;
                Destroy(succSpeedEffect);
                Destroy(this.gameObject,1.5f);
                
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (counter == 0)
        {
            GameObject GO = Instantiate(succSpeedEffect, this.transform);
            GO.transform.localScale /= 2;
            counter++;
        }

        if (other.gameObject.layer == 8)
        {
            dragonSuccRef = FindObjectOfType<DragonSucc>();
            dragonSuccRef.BuffTimerText.gameObject.SetActive(true);
            dragonSuccRef.HasSpeedBuff = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            dragonSuccRef.HasSpeedBuff = false;
        }

    }
}
