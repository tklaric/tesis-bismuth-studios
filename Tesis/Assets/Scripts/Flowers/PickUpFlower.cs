﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpFlower : MonoBehaviour
{

    [SerializeField] KeyCode pickUpKey;
    [SerializeField] int flowerType;
    [SerializeField] GameObject flower;

    private Player playerRef;

    private bool flowerUsed;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(pickUpKey))
        {
            playerRef.GetFlower(flowerType);

            playerRef.FlowerText.SetActive(false);
            flower.SetActive(false);
            flowerUsed = true;           
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            playerRef = other.GetComponentInChildren<Player>();

            if (flowerUsed == false)
            {
                playerRef.FlowerText.SetActive(true);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            playerRef = other.GetComponentInChildren<Player>();

            playerRef.FlowerText.SetActive(false);
        }
        
    }
    private void OnDisable()
    {
        if (playerRef == null) return;
        playerRef.FlowerText.SetActive(false);
    }

}
