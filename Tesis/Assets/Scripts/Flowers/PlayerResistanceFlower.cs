﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerResistanceFlower : MonoBehaviour
{
    private int counter;
    private float timer = 30;
    private bool flowerInstantiated;
    private Player playerRef;
    [SerializeField] GameObject playerResistanceEffect;

    private void Awake()
    {  
        flowerInstantiated = true;
    }

    private void Update()
    {
        if (flowerInstantiated && playerRef != null) 
        {
            timer -= Time.deltaTime;         
            playerRef.BuffTimerText.text = timer.ToString("f0");
            if (timer <= 0)
            {
                timer = 0;
                playerRef.BuffTimerText.gameObject.SetActive(false);
                playerRef.HasResistanceBuffed = false;
                Destroy(playerResistanceEffect);
                Destroy(this.gameObject,1.5f);

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (counter==0)
        {
            GameObject GO = Instantiate(playerResistanceEffect, this.transform);
            GO.transform.localScale /= 2;
            counter++;
        }

        if (other.gameObject.layer == 8)
        {
            playerRef = other.GetComponentInChildren<Player>();
            playerRef.BuffTimerText.gameObject.SetActive(true);
            playerRef.HasResistanceBuffed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            playerRef.HasResistanceBuffed = false;
        }

    }

}
