﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunFlower : MonoBehaviour
{
    private int counter;
    private float timer = 30;
    private bool flowerInstantiated;
    private ShotgunShoot shotgunShootRef;
    [SerializeField] GameObject shotgunFlowerEffect;

    private void Awake()
    {
        flowerInstantiated = true;
    }

    private void Update()
    {
        if (flowerInstantiated && shotgunShootRef != null)
        {
            timer -= Time.deltaTime;
            shotgunShootRef.BuffTimerText.text = timer.ToString("f0");
            if (timer <= 0)
            {
                timer = 0;
                shotgunShootRef.BuffTimerText.gameObject.SetActive(false);
                shotgunShootRef.SpeedIsBuffed = false;
                Destroy(shotgunFlowerEffect);
                Destroy(this.gameObject,1.5f);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (counter == 0)
        {
            GameObject GO = Instantiate(shotgunFlowerEffect, this.transform);
            GO.transform.localScale /= 2;
            counter++;
        }
      
        if (other.gameObject.layer == 8)
        {
            shotgunShootRef = other.GetComponentInChildren<ShotgunShoot>();
            shotgunShootRef.BuffTimerText.gameObject.SetActive(true);
            shotgunShootRef.SpeedIsBuffed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            shotgunShootRef.SpeedIsBuffed = false;
        }

    }
}
