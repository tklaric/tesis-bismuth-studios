﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningDecal : MonoBehaviour
{

    private MeshRenderer mesh;
    [SerializeField] Color color;
    private float decreaseSpeed;

    void Awake()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    
    void Update()
    {
        color.a = Mathf.Abs(Mathf.Sin(Time.time))*0.2f;
        mesh.material.SetColor("_TintColor", color);
       
    }
}
