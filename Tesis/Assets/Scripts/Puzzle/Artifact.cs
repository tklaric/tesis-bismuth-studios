﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using THOR;

public class Artifact : MonoBehaviour
{
    [SerializeField] GameObject lightBeam;
    [SerializeField] GameObject InteractText; 
    [SerializeField] Player player_reference;
    [SerializeField] GameObject thunderEffect;
    [SerializeField] ParticleSystem dustStorm;
    [SerializeField] Color berserkColor;
    [SerializeField] List<Transform> thunderEffectPositions = new List<Transform>();

    private float timer;
    public bool altarHasRised;

    public bool AltarHasRised { get => altarHasRised; set => altarHasRised = value; }

    private void OnTriggerStay(Collider other)
    {
        if (altarHasRised==false)
        {
            InteractText.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (other.tag == "Player");
            {
                //Player playerRef = other.GetComponent<Player>();
                //playerRef.EnablePlayerEnemySpawner();
                lightBeam.SetActive(true);
                StartCoroutine(SpawnEffects());
                player_reference.Speed = 3f;
                //player_reference.CarUpgrades[0] = true;
                player_reference.hasPuzzleArtifact = true;
                SaveAltarEvent();
                AltarHasRised = true;
                InteractText.SetActive(false);
                Destroy(this.gameObject,2f);
            }
        }
    }

    IEnumerator SpawnEffects()
    {
        for (int i = 0; i < thunderEffectPositions.Count; i++)
        {
            Vector3 thunderRotation = new Vector3(0, 198.8f, 0);
            GameObject thunderEffectPrefab = Instantiate(thunderEffect, thunderEffectPositions[i].position, Quaternion.Euler(thunderRotation));
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SaveAltarEvent()
    {
        PlayerMovement player = FindObjectOfType<PlayerMovement>();
        SaveData.current.SaveAltarEvent(player_reference.hasPuzzleArtifact, player.transform.position);
        SerializationManager.Save("SaveData", SaveData.current);
    }
    private void OnTriggerExit(Collider other)
    {
        InteractText.SetActive(false);
    }
}
