﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarRaise : MonoBehaviour
{
    [SerializeField] SpinFloating spinFloatingRef;
    private MeshCollider meshColliderRef;
    private void OnEnable()
    {
        spinFloatingRef.enabled = true;
        meshColliderRef = GetComponentInChildren<MeshCollider>();
        meshColliderRef.enabled = true;
        //SONIDO DE Q SE ELEVA EL ALTAR.
    }
    void Update()
    {
        if (transform.localPosition.y >= -57.7f)
        {
            enabled = false;
        }
        else
        {
            transform.position += new Vector3(0, 0.015f, 0);
        }
        
    }
}
