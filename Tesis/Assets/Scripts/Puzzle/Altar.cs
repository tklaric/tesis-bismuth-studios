﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : MonoBehaviour
{
    [SerializeField] BloodBucket bloodBucketReference;

    private void OnTriggerStay(Collider other)
    {
        bloodBucketReference.isInPosition = true;
        if (Input.GetMouseButtonDown(0))
        {
            if (bloodBucketReference.enabled == true)
            {
                bloodBucketReference.Use();
            }                   
        }    
    }
    private void OnTriggerExit(Collider other)
    {
        bloodBucketReference.isInPosition = false;
    }
}
