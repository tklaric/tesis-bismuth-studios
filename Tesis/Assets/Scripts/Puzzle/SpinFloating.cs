﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinFloating : MonoBehaviour
{
    [SerializeField] float minMax = 1;
    [SerializeField] float sineAmplitudeSpeedMultiplier;
    [SerializeField] float speenSpeed;
    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(0f, Mathf.Sin(sineAmplitudeSpeedMultiplier*(Time.time))*minMax*0.1f, 0f);
        transform.Rotate(0, speenSpeed * Time.deltaTime, 0,Space.World);
    }
}
