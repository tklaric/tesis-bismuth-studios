﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SprintTip : MonoBehaviour
{
    [SerializeField] float speedToShowText;
    [SerializeField] float speedToShowImage;
    [SerializeField] Image sprintImage;
    [SerializeField] TextMeshProUGUI sprintText;
    [SerializeField] PlayerMovement playermov;
    private Color imageColor;
    private Color textColor;

    private void Start()
    {
        imageColor = sprintImage.color;
        textColor = sprintText.color;
        imageColor.a = 0;
        textColor.a = 0;
        sprintImage.color = imageColor;
        sprintText.color = textColor;
    }

    private void Update()
    {
        FadeIn();
    }

    public void FadeIn()
    {       
        textColor.a += Time.deltaTime / speedToShowText;
        if(imageColor.a < 0.85f)
        {
            imageColor.a += Time.deltaTime / speedToShowImage;
        }
        else
        {
            imageColor.a = 0.85f;           
        }
        sprintImage.color = imageColor;
        sprintText.color = textColor;
    }
}
