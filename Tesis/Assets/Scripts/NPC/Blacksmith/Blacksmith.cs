﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blacksmith : MonoBehaviour
{
    [SerializeField] Image _smithy;

    public void SetActiveSmithy()
    {
        _smithy.gameObject.SetActive(true);
    }
}
