﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradientTest : MonoBehaviour
{
    [SerializeField] Color color1;
    [SerializeField] Color color2;
    [SerializeField] Material yourGradientMaterial;
    private Image myImage;

    public Material YourGradientMaterial { get => yourGradientMaterial; set => yourGradientMaterial = value; }
    public Color Color1 { get => color1; }
    public Color Color2 { get => color2; }

    void Start()
    {
        myImage = GetComponent<Image>();
        YourGradientMaterial.SetColor("_Color", Color1); 
        YourGradientMaterial.SetColor("_Color2", Color2);
        myImage.fillAmount = 0;
    }
    public void FillImage(float currentNumber, float maxNumber)
    {
        myImage.fillAmount = currentNumber / maxNumber;
        
        
    }
    public void CompleteLock()
    {
        if (myImage.fillAmount == 1)
        {
            yourGradientMaterial.SetColor("_Color", Color2);
        }
        else
        {
            yourGradientMaterial.SetColor("_Color", Color1);
        }
    }
}
