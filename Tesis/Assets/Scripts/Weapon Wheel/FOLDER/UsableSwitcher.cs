﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsableSwitcher : MonoBehaviour
{
    [SerializeField] private Usable[] Usables = new Usable[8];

    private int m_currentUsableIndex = 0;
    private Usable m_currentUsable;

    public Usable CurrentUsable => m_currentUsable;

    public static UsableSwitcher instance;
    private int indexReference = 0;

    private void Start()
    {
        instance = this;
        m_currentUsableIndex = 0;
        m_currentUsable = Usables[0];
        SwitchUsable(0);
    }

    public void SwitchUsable(int index)
    {
        indexReference = index;
        //Sets our current Weapon
        if (index > Usables.Length)
        {
            return;
        }
        m_currentUsableIndex = index;
        for (int i = 0; i < Usables.Length; ++i)
        {
            if (Usables[i] == null)
                break;
            if (i != m_currentUsableIndex)
            {
                //Disable Usable
                Usables[i].gameObject.SetActive(false);
            }
            else
            {
                //Enable Usable
                Usables[i].gameObject.SetActive(true);
                m_currentUsable = Usables[i];
            }
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            SwitchUsable(5);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            SwitchUsable(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3)) //2 skippeado debido a bug extraño.
            SwitchUsable(3);
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            SwitchUsable(4);
        else if (Input.GetKeyDown(KeyCode.Alpha5)) //5 y 1 switcheados para aprecer con libro pero tocar 1 e ir a escopeta.
            SwitchUsable(0);
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            SwitchUsable(6);
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            SwitchUsable(7);
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            SwitchUsable(8);
        //else if (Input.GetKeyDown(KeyCode.Alpha9))
        //    SwitchUsable(8);
    }
    public void DeactivateCurrentTool()
    {
        CurrentUsable.gameObject.SetActive(false);
    }
    
}
