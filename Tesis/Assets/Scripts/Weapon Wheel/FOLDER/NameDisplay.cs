﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NameDisplay : MonoBehaviour
{
    private TextMeshProUGUI _text;


    // Start is called before the first frame update
    void Start()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_text == null || UsableSwitcher.instance == null || UsableSwitcher.instance.CurrentUsable == null)
            return;

        _text.text = UsableSwitcher.instance.CurrentUsable.Name;

    }
}
