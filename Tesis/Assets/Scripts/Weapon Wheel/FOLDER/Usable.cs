﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usable : MonoBehaviour
{
    public int Index;
    public string Name;

    public Usable(int _index = 0, string _name = "Item1")
    {
        Index = _index;
        Name = _name;
    }  
}
